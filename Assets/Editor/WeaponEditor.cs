﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Weapon), true)]
[CanEditMultipleObjects]
public class WeaponEditor : Editor {
    public List<string> ammoList;
    public int index = 0;
    string[] ammoOptions;
    Weapon myWeapon;

	public override void OnInspectorGUI() {
        myWeapon = (Weapon)target;
        ammoList = new List<string>();
        GeneralParameters[] generalParameters = (GeneralParameters[])Resources.FindObjectsOfTypeAll(typeof(GeneralParameters));
        if (generalParameters.Length > 0) {
            //Here we get ammo default Data list
            Caliber[] ammoCalibers = generalParameters[0].defaultAmmoInventory;
            //Building our string[] for dropdown menu
            foreach (Caliber ammoCaliber in ammoCalibers) {
                ammoList.Add(ammoCaliber.name);
            }

            DrawDefaultInspector();
            EditorGUILayout.LabelField("Custom settings", EditorStyles.boldLabel);
            ammoOptions = ammoList.ToArray();
            EditorGUI.BeginChangeCheck();
            index = EditorGUILayout.Popup("Change Ammo Type", index, ammoOptions);
            if (EditorGUI.EndChangeCheck())
                myWeapon.ammoType = ammoOptions[index];
        }
        else {
            DrawDefaultInspector();
            //Debug.LogWarning("WeaponEditor: no generalParameters object found!");
        }
    }
}
