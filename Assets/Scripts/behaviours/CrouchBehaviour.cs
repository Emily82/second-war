﻿using UnityEngine;

public class CrouchBehaviour : StateMachineBehaviour {

	private Movement movement;

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		movement = animator.GetComponent<Movement>();
		animator.SetBool("run", false);
		movement.OnCrouchEvent();
	}

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		movement.OnStandUpEvent();
	}
}
