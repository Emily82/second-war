﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct WeaponPreview {
	public Sprite image;
	public GameObject weaponPrefab;
}

public class SelectWeaponMenu : Menu {
	public bool disableAfterAwake = true;
	public WeaponPreview[] weapons;

	private Image preview;
	private Dropdown dropdown;

	public static SelectWeaponMenu instance;

	void Awake() {
		instance = this;
		preview = transform.Find("Preview").GetComponent<Image>();
		dropdown = GetComponentInChildren<Dropdown>();
		if (disableAfterAwake)
			gameObject.SetActive(false);
	}

	public void setWeapon() {
		//NOTE: remember to setup weapons in SelectWeaponMenu script inspector!
		preview.sprite = weapons[dropdown.value].image;
		MapLoader.instance.setPlayerWeapon(weapons[dropdown.value]);
	}
}
