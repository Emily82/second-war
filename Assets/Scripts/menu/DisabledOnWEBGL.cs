﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisabledOnWEBGL : MonoBehaviour {
	void Awake() {
		#if UNITY_WEBGL
			gameObject.SetActive(false);
		#endif
	}
}
