﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Menu : MonoBehaviour {
	//Hides current menu showing new menu
	public void GoTo(GameObject menu) {
		if (menu) {
			gameObject.SetActive(false);
			menu.SetActive(true);
		}
		else
			Debug.LogError(this.GetType().Name + ": menu " + menu +" is null!");
	}
}
