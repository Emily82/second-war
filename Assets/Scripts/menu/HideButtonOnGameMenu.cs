﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideButtonOnGameMenu : MonoBehaviour {
	
	void Awake() {
		if (isInGame())
			gameObject.SetActive(false);
	}

	private bool isInGame() {
		//It is assumed there is at least one parent with Modal component
		return GetComponentInParent<Modal>().InGame;
	}
}
