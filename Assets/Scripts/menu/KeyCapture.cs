﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCapture : MonoBehaviour {
	public static KeyCapture instance;
	public bool disableAfterAwake = true;

	//key name customized by the user
	private string keyName;
	private int index; //NOTE: this is also the name of clicked button
	private bool capture = false;

	void Awake() {
		instance = this;
		if (disableAfterAwake)
			gameObject.SetActive(false);
	}

	public void Show(string keyName, int index) {
		this.keyName = keyName;
		this.index = index;
		KeyboardSettingsMenu.instance.gameObject.SetActive(false);
		gameObject.SetActive(true);
		capture = true;
	}

	private void Close() {
		KeyboardSettingsMenu.instance.gameObject.SetActive(true);
		KeyboardSettingsMenu.instance.updateCustomizedBind(keyName, index.ToString());
		gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (capture) {
			foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode))) {
				if (Input.GetKey (vKey)) {
					SettingsManager.SetBind(keyName, vKey, index);
					capture = false;
					Close();
					break;
				}
         	}
		}
	}
}
