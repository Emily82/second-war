﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenu : Menu {
	public bool disableAfterAwake = true;

	public static SettingsMenu instance;

	void Awake() {
		instance = this;
		if (disableAfterAwake)
			gameObject.SetActive(false);
	}
}
