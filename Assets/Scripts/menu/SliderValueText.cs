﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValueText : MonoBehaviour
{
    [SerializeField]
    [Tooltip("The text shown will be formatted using this string.  {0} is replaced with the actual value")]
    private string formatText = "{0:0.0}";

    private Text sliderText;
    private Slider slider;

    void Awake()
    {
        sliderText = GetComponent<Text>();
        slider = GetComponentInParent<Slider>();

        //Retrieve user data and set to slider
        if (PlayerPrefs.HasKey(slider.name))
            slider.value = PlayerPrefs.GetFloat(slider.name);
        else //Create the key with slider default value
            PlayerPrefs.SetFloat(slider.name, slider.value);

        sliderText.text = string.Format(formatText, slider.value);
        slider.onValueChanged.AddListener(HandleValueChanged);
    }

    private void HandleValueChanged(float value)
    {
        sliderText.text = string.Format(formatText, value);
        //Store data to user preferences
        SettingsManager.SetParameter(slider.name, value);
    }
}
