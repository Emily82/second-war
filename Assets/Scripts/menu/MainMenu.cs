﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MainMenu : Menu {

	public static MainMenu instance;

	//Singleton
	public void Awake() {
		instance = this;
		SettingsManager.Initialize();
	}

	public void Quit() {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}

	public void StartScene(string sceneName) {
		SceneManager.LoadScene(sceneName);
	}

	//For IngameMenu
	public void ReturnToGame() {
		UIKeyboardListener.instance.hideIngameMenu();
	}
}
