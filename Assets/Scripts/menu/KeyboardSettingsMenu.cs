﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyboardSettingsMenu : Menu {
	public static KeyboardSettingsMenu instance;
	public bool disableAfterStart = true;
	HorizontalLayoutGroup[] map;

	void Awake() {
		instance = this;
	}

	void Start() {
		map = GetComponentsInChildren<HorizontalLayoutGroup>();
		updateKeyboardBinds();
		if (disableAfterStart)
			gameObject.SetActive(false);
	}

	//Setup all keyboard buttons for user interface
	void updateKeyboardBinds() {
		foreach (HorizontalLayoutGroup obj in map) {
			KeyCode[] binds = SettingsManager.GetBinds(obj.name);
			if (binds != null) {
				Button[] buttons = obj.GetComponentsInChildren<Button>();
				foreach (Button button in buttons) {
					int index = int.Parse(button.name);
					if (index < binds.Length) {
						button.GetComponentInChildren<Text>().text = binds[index].ToString();
						button.onClick.RemoveAllListeners();
						button.onClick.AddListener(
							() => {
								//Pass all data to capture popup script and show its UI
								KeyCapture.instance.Show(obj.name, index);
							}
						);
					}
				}
			}
		}
	}

	public void Default() {
		SettingsManager.ResetKeyPlayerPrefs();
		updateKeyboardBinds();
	}

	//keyName : key to bind  buttonName : index (the name of clicked button)
	public void updateCustomizedBind(string keyName, string buttonName) {
		foreach (HorizontalLayoutGroup obj in map) {
			KeyCode[] binds = SettingsManager.GetBinds(keyName);
			if (obj.name == keyName) {
				Button[] buttons = obj.GetComponentsInChildren<Button>();
				foreach (Button button in buttons) {
					if (button.name == buttonName) {
						int index = int.Parse(buttonName);
						button.GetComponentInChildren<Text>().text = binds[index].ToString();
					}
				}
			}
		}
	}
}
