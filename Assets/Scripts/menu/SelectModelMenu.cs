﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct ModelPreview {
	public Sprite image;
	public GameObject modelPrefab;
	public Team team;
}

public class SelectModelMenu : Menu {
	public bool disableAfterAwake = true;
	public ModelPreview[] models;

	private Image preview;
	private Dropdown dropdown;

	public static SelectModelMenu instance;

	void Awake() {
		instance = this;
		preview = transform.Find("Preview").GetComponent<Image>();
		dropdown = GetComponentInChildren<Dropdown>();
		if (disableAfterAwake)
			gameObject.SetActive(false);
	}

	public void setModel() {
		//NOTE: remember to setup models in SelectModelMenu script inspector!
		preview.sprite = models[dropdown.value].image;
		MapLoader.instance.setPlayerPrefab(models[dropdown.value]);
	}
}
