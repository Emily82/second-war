﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Modal : MonoBehaviour {
	[Tooltip("If true disables Menu on startup.")]
	public bool InGame = false;
	public static Modal instance;

	//Init singleton
	void Awake() {
		instance = this;
		if (InGame)
			transform.parent.gameObject.SetActive(false);
	}

	public void Toggle(UnityAction showCode = null, UnityAction hideCode = null) {
		if (transform.parent.gameObject.activeSelf)
			Hide(hideCode);
		else
			Show(showCode);
	}

	//code its the code to execute when showing / hiding the modal
	public void Show(UnityAction code = null) {
		if (code != null)
			code();
		transform.parent.gameObject.SetActive(true);
	}

	public void Hide(UnityAction code = null) {
		if (code != null)
			code();
		transform.parent.gameObject.SetActive(false);
	}
}
