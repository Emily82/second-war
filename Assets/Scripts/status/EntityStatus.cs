using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Inventory = PrintableDictionary<string, int>;

//Attached to "Player" gameobject or "BotPlayer" gameobject
public class EntityStatus : MonoBehaviour {
	public bool inert = false;
	//TODO: not nice solution as public field
	[SerializeField]
	public Team team;
	[Tooltip("Disable camera components for this entity (normally bots)")]
	public bool disableCameras = false;
	
	public bool IsAlive {
		get {
			if (shootable)
				return (shootable.currentHealth > 0);
			else
				return false;
		}
	}
	public bool GodMode {get; private set;}
	public bool IsPlayer {
		get {
			return isPlayer;
		}
	}

	private Inventory ammo;
	private GeneralParameters generalParameters;
	private MapLoader mapLoader;
	private Shootable shootable;
	private Animator animator;
	private WeaponControl weaponControl;
	private Weapon weapon;
	private Camera fspCameraComponent;
	private Camera bonesCameraComponent;
	private float timer = 0;
	private Canvas iconCanvas;
	private GameObject godModeIcon;
	private bool isPlayer;
	//private int debugAmmo;

	void Awake() {
		ammo = new Inventory();
		GodMode = true;
	}

	void Start() {
		generalParameters = GeneralParameters.instance;
		mapLoader = MapLoader.instance;
		shootable = GetComponentInChildren<Shootable>();
		animator = GetComponent<Animator>();
		weaponControl = GetComponent<WeaponControl>();
		weapon = GetComponentInChildren<Weapon>();
		fspCameraComponent = GetComponentInChildren<Camera>();
		bonesCameraComponent = shootable.GetComponentInChildren<Camera>();
		//If this is not null then this entitystatus is owned by a player entity
		isPlayer = (GetComponent<PlayerMouseCamera>() != null);
		if (!isPlayer) {
			//only bots and non player entities have got icon in transform hierarchy
			iconCanvas = GetComponentInChildren<Canvas>();
		}
		else {
			//player's iconCanvas is UI gameobject (tagged "UserInterface")
			iconCanvas = GameObject.FindGameObjectWithTag("UserInterface").GetComponent<Canvas>();
		}
		godModeIcon = iconCanvas.transform.Find("Godmode").gameObject;
		restoreAmmoInventory();
		if (disableCameras)
			disableBothCameras();
	}

	//This function will be called by MouseCamera
	public void updateGun() {
		weapon = GetComponentInChildren<Weapon>();
	}

	void Update() {
		if (GodMode) {
			timer += Time.deltaTime;
			if (timer >= mapLoader.respawnImmunityTime) {
				GodMode = false;
				timer = 0;
				//Hide GodMode icon
				godModeIcon.SetActive(false);
			}
		}
	}

	//Restore ammo values from entityLoader.defaultAmmoEquipment
	public void restoreAmmoInventory() {
		foreach(Caliber defaultAmmo in generalParameters.defaultAmmoInventory)
			ammo[defaultAmmo.name] = defaultAmmo.amount;
	}

	//Used by console command
	public void printAmmo(string type) {
		if (ammo.ContainsKey(type))
			print(ammo[type]);
		else
			print("EntityStatus: the ammo type " + type + " does not exist.");
	}

	public int getAmmo(string name) {
		return ammo[name];
	}

	public void setAmmo(string name, int amount) {
		ammo[name] = amount;
	}

	public void useAmmo(string name, int amount) {
		ammo[name] -= amount;
	}

	public void Die() {
		if (!inert) {
			//Trigger Death animation
			weaponControl.ikActive = false;
			weapon.Leave();
			//Change to death camera disabling fixed camera
			if (!disableCameras)
				setViewInsideBones();
			animator.SetTrigger("die");
		}
		else //Simply trash this inert object
			Pooling.TrashGameObject(gameObject);
	}

	public void Revive() {
		GodMode = true;
		//Show GodMode icon
		godModeIcon.SetActive(true);
		//This should change to entry state -> idle
		animator.SetTrigger("respawn");
		//Return to default fixed camera
		if (!disableCameras)
			setViewOutsideBones();
		//This will set the entity alive again
		shootable.RestoreHealth();
		weaponControl.ikActive = true;
		weapon.Take();
	}

	private void setViewInsideBones() {
		fspCameraComponent.enabled = false;
		bonesCameraComponent.enabled = true;
	}

	private void setViewOutsideBones() {
		fspCameraComponent.enabled = true;
		bonesCameraComponent.enabled = false;
	}

	private void disableBothCameras() {
		fspCameraComponent.enabled = false;
		bonesCameraComponent.enabled = false;
	}
}
