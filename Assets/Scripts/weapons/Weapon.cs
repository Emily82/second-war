﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//General abstract weapon class.
public abstract class Weapon : MonoBehaviour {
	[Header("Damage")]
    public int gunDamage = 100;
	[Header("Weapon Rate")]
	public float fireRate = 1.5f;
	public float bashRate = 1f;
	[Header("Weapon Range and Impact Force")]
	public float weaponRange = 600f;
	public float bashRange = 1.5f;
	public float hitForce = 100f;
	[Tooltip("Amount of ammo per cartridge")]
	public int cartridgeAmmo = 5;
	[Header("General Audio clips")]
	[Tooltip("Weapon shooting audio clip")]
	public AudioClip shootClip;
	[Tooltip("Weapon reloading audio clip")]
	public AudioClip reloadClip;
	[Tooltip("Weapon bashing audio clip")]
	public AudioClip bashClip;
	[Header("Recoil options")]
	[Tooltip("Weapon Degrees vertical rotation for recoil")]
	public float recoilPitch = 3;
	[Tooltip("Weapon max Degrees horizontal random rotation for recoil")]
	public float recoilYaw = 0;
	[Tooltip("Recoil restore degrees per seconds")]
	public float recoilRecoveryStep = 0.1f;
	[Tooltip("Ammo type used by this weapon")]
	public string ammoType;

	protected AudioSource gunAudio;
	protected AudioSource reloadAudio;
	protected AudioSource bashAudio;
	//Shoot input trigger signal
    protected bool fireTrigger = false;
	//Reload input signal (used for only manual reload)
	protected bool reloadTrigger = false;
	//Bash input signal
	protected bool bashTrigger = false;
	//Variables used and implemented in child classes
	protected int defaultCartridgeAmmo;
	protected Transform gunEnd;
	protected Camera fpsCamera;
	//Next timepoint to enable back shooting (fireRate related)
	protected float nextFire;
	protected float nextBash;
	//Reference to root (child classes will set it)
	protected GameObject rootGameObject;
	protected bool isShooting = false;
	//Flag activated when raycast collides a hurtbox making real damage
	protected bool isHurtboxDamaged = false;
	protected Animator gunAnimator;
	protected EntityStatus entityStatus;
	protected MouseCamera mouseCamera;
	protected ParticleSystem muzzleFlash;
	//Usefull to replace the weapon in default position
	protected Vector3 defaultPos;
	protected Quaternion defaultRot;
	protected Rigidbody weaponRigidbody;
	protected Movement movement;

	//Returns the owner gameobject of this weapon
	public GameObject Owner {
		get {
			return rootGameObject;
		}
	}

	//When reloading this flag will prevent doing any other action
	public bool IsReloading {get; private set;}

	//From public we can only read the value of protected var isHurtboxDamaged (inherited)
	public bool IsHurtboxDamaged {
		get {
			return isHurtboxDamaged;
		}
	}

	//This property helps us to understand if fire signal has been reset (when shooting really ends)
	public bool isFireReset {
		get {
			return !fireTrigger;
		}
	}

	//Received reload signal
	public void Reload() {
		reloadTrigger = true;
	}
		
	//Received shoot signal
	public void Shoot() {
		fireTrigger = true;
	}

	//Received bash signal (if current weapon does not support bash, this signal will be ignored)
	public void Bash() {
		bashTrigger = true;
	}

	//Reset all trigger signals
	protected void resetAllSignals() {
		fireTrigger = false;
		bashTrigger = false;
		reloadTrigger = false;
	}

	//To be called only on respawn
	public void RestoreCartridge() {
		cartridgeAmmo = defaultCartridgeAmmo;
	}

	//Restore default position and rotation
	public void Take() {
		weaponRigidbody.isKinematic = true;
		transform.localPosition = defaultPos;
		transform.localRotation = defaultRot;
		enableAnimator();
	}

	//Falling weapon effect
	public void Leave() {
		disableAnimator();
		weaponRigidbody.isKinematic = false;
	}

	public abstract bool isAutomatic();

	//Disables/Enables current weapon's animator
	public void disableAnimator() {
		gunAnimator.enabled = false;
	}
	public void enableAnimator() {
		gunAnimator.enabled = true;
		//We need to play Idle default state to avoid playing the state before last disable
		gunAnimator.Play("Idle", -1);
	}

	//When weapon is changed, this method updates all references to new owner
	public void updateOwner() {
		entityStatus = GetComponentInParent<EntityStatus>();
		rootGameObject = entityStatus.gameObject;
		mouseCamera = rootGameObject.GetComponent<MouseCamera>();
		movement = rootGameObject.GetComponent<Movement>();
		fpsCamera = GetComponentInParent<Camera>();
	}

	protected void updateAudioSettings() {
		gunAudio = gameObject.AddComponent<AudioSource>();
		gunAudio.clip = shootClip;
		GeneralParameters.setAudioSettings(ref gunAudio);

		reloadAudio = gameObject.AddComponent<AudioSource>();
		reloadAudio.clip = reloadClip;
		GeneralParameters.setAudioSettings(ref reloadAudio);
	}

	//Runs in child classes
	protected void Awake() {
		//DefaultCartridgeAmmo must be set only once, before the game
		defaultCartridgeAmmo = cartridgeAmmo;

		//Save default transform
		defaultPos = new Vector3(transform.localPosition.x,
								 transform.localPosition.y,
								 transform.localPosition.z);
		defaultRot = new Quaternion(transform.localRotation.x,
									transform.localRotation.y,
									transform.localRotation.z,
									transform.localRotation.w);

		//Create all needed audiosources and preloading clips
		updateAudioSettings();

		//These must be placed in awake to avoid being not initialized in time!
		gunAnimator = GetComponent<Animator>();
		weaponRigidbody = GetComponent<Rigidbody>();
	}

	//Runs in child classes
	protected void Start() {
		updateOwner();
		gunEnd = transform.Find("GunEnd").transform;
		muzzleFlash = GetComponentInChildren<ParticleSystem>();
	}
	
	//Reload ammo playing sound and animation effects
	protected IEnumerator ReloadAmmo() {
		IsReloading = true;
		//Trigger reload animation
		gunAnimator.SetTrigger("Reload");
		//Reload
		reloadAudio.Play();
		//Dont restore reloadAmmo until reload audioclip end
		yield return new WaitForSecondsRealtime(reloadAudio.clip.length);

		//Update new ammo and cartridge ammo values
		int neededAmmo = defaultCartridgeAmmo - cartridgeAmmo;
		if (entityStatus.getAmmo(ammoType) >= neededAmmo) {
			entityStatus.useAmmo(ammoType, neededAmmo);
			cartridgeAmmo = defaultCartridgeAmmo;
		}
		else {
			//Putting remaining ammo in cartridge
			cartridgeAmmo += entityStatus.getAmmo(ammoType);
			entityStatus.setAmmo(ammoType, 0);
		}
		IsReloading = false;
	}
}
