using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class will not be attached to any gameobject
public class BulletWeapon : Weapon {
	[Header("General options")]
	[Tooltip("Set to true for automatic weapons like submachine guns, false otherwise.")]
	public bool automatic = false;
	[Tooltip("Is meele weapon? (example knife).")]
	public bool meele = false;
	[Tooltip("Can bash with this weapon?")]
	public bool bashable = true;
	[Tooltip("Can zoom with this weapon?")]
	public bool sniper = false;
	[Header("Karabine kind weapon options")]
	[Tooltip("The more you increase this time adjust, the earlier rechamber sound will be played.")]
	public float rechamberAdjust = 0.5f;
	[Tooltip("Weapon rechamber audio clip. Leave empty if no rechamber for this weapon.")]
	public AudioClip rechamberClip;
	
	private AudioSource rechamberAudio;
	private WaitForSeconds shotDuration = new WaitForSeconds(.04f);
	//Layermask for raycast
	private int layerMask;
	//Line renderer (bullet trace) start - end points positions
	private LineRenderer bulletLine;
	private Vector3 lineStart;
	private Vector3 lineEnd;

	new void Awake() {
		base.Awake();
		//Let's check flags constraints
		meele = (bashable && automatic && sniper)? false : meele;
		bashable = (meele && sniper)? false : bashable;
		automatic = (meele)? false : automatic;
		sniper = (meele && bashable)? false : sniper;

		updateAudioSettings();
	}

	//Runs in child classes
	new void Start() {
		base.Start();
		bulletLine = GetComponent<LineRenderer>();
		layerMask = Helper.getLayerMask("Ignore Raycast", "WeaponsLayer", "Pushbox");
	}

	void Update() {
		//Checking signals and building conditions
		bool conditions = false;
		if (fireTrigger && !bashTrigger && !reloadTrigger) {
			conditions = (cartridgeAmmo > 0) &&  
						 (Time.time > nextFire);
		}
		else if (bashTrigger && !fireTrigger && !reloadTrigger) {
			conditions = (Time.time > nextBash);
		}
		if (conditions && !IsReloading && entityStatus.IsAlive) 
		{ //Shoot or Bash!
			//Sound and visual effects
			if (fireTrigger) {
				//Updating shooting rate time
				nextFire = Time.time + fireRate;
				StartCoroutine(FireWeapon());
				doRayCast(weaponRange, gunDamage);
				//NOTE: Mouse recoil effect only after raycast or it will be unaccurate
				mouseCamera.Recoil();
			}
			//Bashing so no bullet use
			 else if (bashTrigger) {
				if (bashable) {
					//Updating bashing rate time
					nextBash = Time.time + bashRate;
					StartCoroutine(BashEffect());
					doRayCast(bashRange, gunDamage / 2);
				}
			}
		}
		//Reload
		else if (!IsReloading && !isShooting) {
			//Reload logic (both auto and manual):
			//( We have ammo ) AND ( ( Weapon cartridge its empty ) XOR ( Manual reload AND cartridge its not full ) )
			if ((entityStatus.getAmmo(ammoType) > 0) && ((cartridgeAmmo <= 0) ^ (reloadTrigger && cartridgeAmmo < defaultCartridgeAmmo))) {
				//Reload sound animation
				StartCoroutine(ReloadAmmo());
				//this could be not needed but to be sure we enforce the trigger reset
				reloadTrigger = false;
			}
			else //Weapon can bw switched if we are not doing anything
				movement.allowSwitchWeapon();
		}
		//Resets current script trigger signals for each frame
		resetAllSignals();
	}

	public override bool isAutomatic() {
		return automatic;
	}

	private new void updateAudioSettings() {
		base.updateAudioSettings();
		if (rechamberClip != null) {
			rechamberAudio = gameObject.AddComponent<AudioSource>();
			rechamberAudio.clip = rechamberClip;
			GeneralParameters.setAudioSettings(ref rechamberAudio);
		}
		if (bashable) {
			bashAudio = gameObject.AddComponent<AudioSource>();
			bashAudio.clip = bashClip;
			GeneralParameters.setAudioSettings(ref bashAudio);
		}
	}
	
	//Do RayCast and call damage function if the hit collider extends ShotableEntity
	private void doRayCast(float range, int damage) {
		//Raycast origin point (needs to be calculated for every frame)
		Vector3 rayOrigin = fpsCamera.ViewportToWorldPoint (new Vector3(.5f, .5f, 0));
		//Additional information on hit point
		RaycastHit hit;
		//RenderLine origin point
		bulletLine.SetPosition(0, gunEnd.position);

		//If we have some collision making raycast
		if (Physics.Raycast(rayOrigin, 
							fpsCamera.transform.forward, 
							out hit, 
							range, 
							layerMask)) 
		{
			//RenderLine dest point
			bulletLine.SetPosition(1, hit.point);
			//Possible object we hit with our weapon
			Hurtbox collidedBox = hit.collider.GetComponent<Hurtbox>();
			//We shot a hurtbox!
			if (collidedBox != null) {
				collidedBox.Damage(damage, rootGameObject.name);
				isHurtboxDamaged = true;
				print(this.GetType().Name + ": Damage!");
			}
			else {
				isHurtboxDamaged = false;
				print(this.GetType().Name + ": No damage!");
			}
			
			//Let's check now if the shot collided object its a rigidbody, if so we should apply a force
			if (hit.rigidbody != null) {
				hit.rigidbody.AddForce(-hit.normal * hitForce);
				print(this.GetType().Name + ": Applying impact force!");
			}
		}
		else { //RenderLine dest point (when no hit point)
			bulletLine.SetPosition(1, fpsCamera.transform.position + fpsCamera.transform.forward * weaponRange);
		}
	}

    //Fire weapon using 1 bullet and plays sound, visual effects
	private IEnumerator FireWeapon() {
		isShooting = true;
		//Activate muzzleFlash
		if (muzzleFlash)
			muzzleFlash.Play();
		//One less bullet
		cartridgeAmmo--;
		//Show LineRender (bullet trace)
		bulletLine.enabled = true;
		yield return shotDuration;
		//Hide
		bulletLine.enabled = false;
		//Deactivate muzzleFlash
		if (muzzleFlash)
			muzzleFlash.Stop();
		//Shoot sound
		gunAudio.Play();
		yield return new WaitForSecondsRealtime(gunAudio.clip.length - rechamberAdjust);
		//If this weapon has got rechamber clip, play it!
		if (rechamberClip != null) {
			rechamberAudio.Play();
			yield return new WaitForSecondsRealtime(rechamberAudio.clip.length);
		}
		isShooting = false;
	}

	//Normally bash its available only for bullet weapons
	//Bash weapon sound effect
	private IEnumerator BashEffect() {
		//Play weapon's bash animation
		gunAnimator.SetTrigger("Bash");
		//Play bash audio clip
		bashAudio.Play();
		yield return new WaitForSecondsRealtime(gunAudio.clip.length);
	}
}
