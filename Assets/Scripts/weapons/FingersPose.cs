﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingersPose : MonoBehaviour {
	public bool enableAdjust = true;
	[Header("Proximal")]
	[Header("Right Fingers Pitch")]
	public float rightThumb1Pitch = 0;
	public float rightIndex1Pitch = 0;
	public float rightMiddle1Pitch = 0;
	public float rightRing1Pitch = 0;
	public float rightLittle1Pitch = 0;
	[Header("Right Fingers Yaw")]
	public float rightThumb1Yaw = 0;
	public float rightIndex1Yaw = 0;
	public float rightMiddle1Yaw = 0;
	public float rightRing1Yaw = 0;
	public float rightLittle1Yaw = 0;
	[Header("Right Fingers Roll")]
	public float rightThumb1Roll = 0;
	public float rightIndex1Roll = 0;
	public float rightMiddle1Roll = 0;
	public float rightRing1Roll = 0;
	public float rightLittle1Roll = 0;
	[Header("Left Fingers Pitch")]
	public float leftThumb1Pitch = 0;
	public float leftIndex1Pitch = 0;
	public float leftMiddle1Pitch = 0;
	public float leftRing1Pitch = 0;
	public float leftLittle1Pitch = 0;
	[Header("Left Fingers Yaw")]
	public float leftThumb1Yaw = 0;
	public float leftIndex1Yaw = 0;
	public float leftMiddle1Yaw = 0;
	public float leftRing1Yaw = 0;
	public float leftLittle1Yaw = 0;
	[Header("Left Fingers Roll")]
	public float leftThumb1Roll = 0;
	public float leftIndex1Roll = 0;
	public float leftMiddle1Roll = 0;
	public float leftRing1Roll = 0;
	public float leftLittle1Roll = 0;

	[Header("Intermediate")]
	[Header("Right Fingers Pitch")]
	public float rightThumb2Pitch = 0;
	public float rightIndex2Pitch = 0;
	public float rightMiddle2Pitch = 0;
	public float rightRing2Pitch = 0;
	public float rightLittle2Pitch = 0;
	[Header("Right Fingers Yaw")]
	public float rightThumb2Yaw = 0;
	public float rightIndex2Yaw = 0;
	public float rightMiddle2Yaw = 0;
	public float rightRing2Yaw = 0;
	public float rightLittle2Yaw = 0;
	[Header("Right Fingers Roll")]
	public float rightThumb2Roll = 0;
	public float rightIndex2Roll = 0;
	public float rightMiddle2Roll = 0;
	public float rightRing2Roll = 0;
	public float rightLittle2Roll = 0;
	[Header("Left Fingers Pitch")]
	public float leftThumb2Pitch = 0;
	public float leftIndex2Pitch = 0;
	public float leftMiddle2Pitch = 0;
	public float leftRing2Pitch = 0;
	public float leftLittle2Pitch = 0;
	[Header("Left Fingers Yaw")]
	public float leftThumb2Yaw = 0;
	public float leftIndex2Yaw = 0;
	public float leftMiddle2Yaw = 0;
	public float leftRing2Yaw = 0;
	public float leftLittle2Yaw = 0;
	[Header("Left Fingers Roll")]
	public float leftThumb2Roll = 0;
	public float leftIndex2Roll = 0;
	public float leftMiddle2Roll = 0;
	public float leftRing2Roll = 0;
	public float leftLittle2Roll = 0;

	[Header("Distal")]
	[Header("Right Fingers Pitch")]
	public float rightThumb3Pitch = 0;
	public float rightIndex3Pitch = 0;
	public float rightMiddle3Pitch = 0;
	public float rightRing3Pitch = 0;
	public float rightLittle3Pitch = 0;
	[Header("Right Fingers Yaw")]
	public float rightThumb3Yaw = 0;
	public float rightIndex3Yaw = 0;
	public float rightMiddle3Yaw = 0;
	public float rightRing3Yaw = 0;
	public float rightLittle3Yaw = 0;
	[Header("Right Fingers Roll")]
	public float rightThumb3Roll = 0;
	public float rightIndex3Roll = 0;
	public float rightMiddle3Roll = 0;
	public float rightRing3Roll = 0;
	public float rightLittle3Roll = 0;
	[Header("Left Fingers Pitch")]
	public float leftThumb3Pitch = 0;
	public float leftIndex3Pitch = 0;
	public float leftMiddle3Pitch = 0;
	public float leftRing3Pitch = 0;
	public float leftLittle3Pitch = 0;
	[Header("Left Fingers Yaw")]
	public float leftThumb3Yaw = 0;
	public float leftIndex3Yaw = 0;
	public float leftMiddle3Yaw = 0;
	public float leftRing3Yaw = 0;
	public float leftLittle3Yaw = 0;
	[Header("Left Fingers Roll")]
	public float leftThumb3Roll = 0;
	public float leftIndex3Roll = 0;
	public float leftMiddle3Roll = 0;
	public float leftRing3Roll = 0;
	public float leftLittle3Roll = 0;
}
