using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;
using IngameDebugConsole;

//Attached in runtime by MapLoader in Main game object
public class Console : MonoBehaviour {
    public static Console instance;

    private GameObject consoleWindow;
    private GameObject inputField;
    private GameObject fps;
    private GameObject player;
    private Weapon weaponScript;
    private PlayerMovement movementScript;
    private PlayerMouseCamera mouseCameraScript;
    private bool interactable = false;

    void Awake() {
        instance = this;
        consoleWindow = GameObject.FindGameObjectWithTag("Console");
        inputField = GameObject.FindGameObjectWithTag("ConsoleInputField");
        fps = GameObject.FindGameObjectWithTag("FPS");
        //By default we hide FPS
        hideFPS();
        //Hide the console
        consoleWindow.SetActive(false);
    }

    //This method is called by MapLoader
    public void initPlayerScripts(Entity playerEntity) {
        player = playerEntity.RootGameObject;
        enableUserInput();
        weaponScript = player.GetComponentInChildren<Weapon>();
        movementScript = player.GetComponent<PlayerMovement>();
        mouseCameraScript = player.GetComponent<PlayerMouseCamera>();
        //Now we can define commands we have all references
        defineCommands();
    }

    private void defineCommands() {
        //Debug general console commands configuration
		DebugLogConsole.AddCommandStatic( "printLimbo", 
										  "Prints a list of disabled entity models ready for reuse (generally rockets and grenades).", 
										  "printLimbo", 
										  typeof( Console ) );
        DebugLogConsole.AddCommandStatic( "printByName", 
										  "Search entity by name and prints some info (example: printByName player)", 
										  "printByName", 
										  typeof( Console ) );
        DebugLogConsole.AddCommandStatic( "printByTag", 
										  "Shows the name of all players with given tag", 
										  "printByTag", 
										  typeof( Console ) );
        //TODO: Admin
        DebugLogConsole.AddCommandStatic( "resetLimbo", 
										  "Resets the pooling limbo destroying all referenced gameobjects. WARNING, this command can overhead the game, use it just for debug!", 
										  "resetLimbo", 
										  typeof( Pooling ) );
        DebugLogConsole.AddCommandStatic( "destroyByTag", 
										  "Destroy all gameobjects by tag", 
										  "destroyByTag", 
										  typeof( Console ) );
        DebugLogConsole.AddCommandStatic( "setPosition", 
										  "Change position to an entity (example: setPosition player [0 40 0])", 
										  "setPosition", 
										  typeof( Console ) );
        DebugLogConsole.AddCommandStatic( "setEulerAngles", 
										  "Change rotation angles to an entity (example: setEulerAngles player [90 0 0])", 
										  "setEulerAngles", 
										  typeof( Console ) );
        //Commands calling component's functions
        DebugLogConsole.AddCommandInstance( "printMapBounds", "Ground map size.", "printMapBounds", MapLoader.instance );
		DebugLogConsole.AddCommandInstance( "sensitivity", "Set mouse sensitivity", "sensitivity", this);
        DebugLogConsole.AddCommandInstance( "setWeapon", "Set player's weapon, it will change on next respawn", "setPrimaryPrefabWithName", player.GetComponentInChildren<WeaponLoader>() );
        DebugLogConsole.AddCommandInstance( "suicide", "Commit suicide", "suicide", player.GetComponentInChildren<ShootableEntity>() );
        DebugLogConsole.AddCommandInstance( "showFPS", "Show FPS panel", "showFPS", this );
        DebugLogConsole.AddCommandInstance( "hideFPS", "Hide FPS panel", "hideFPS", this );

        //TODO: Admin
        DebugLogConsole.AddCommandInstance( "speed", "Set player's speed", "speed", movementScript);
        DebugLogConsole.AddCommandInstance( "jumpSpeed", "Set player's jump height", "jumpSpeed", movementScript);
        DebugLogConsole.AddCommandInstance( "restart", 
						                    "Restart the current map / scene. Example: restart 1. Restarts game in TeamDeathMatch mode.", 
										    "restart", 
										    MapLoader.instance);
    }

    private void enableUserInput() {
        interactable = true;
    }

    void Update() {
        //Player keyboard control
        if (interactable && SettingsManager.GetButtonDown("Console")) {
            //Hide/Show console
            consoleWindow.SetActive(!consoleWindow.activeSelf);
            //Mouse cursor handling with console
            if (consoleWindow.activeSelf)
                Helper.showCursor();
            else
                Helper.hideCursor();
            //Toggle player input scripts
            Helper.toggleUserInput(weaponScript, movementScript, mouseCameraScript);
            if (consoleWindow.activeSelf) {
                //Wait console loads on before focusing the text field!
                Invoke("focusInput", 1f);
            }
        }
    }

    //Called by WeaponLoader to update gun reference!!!
    public void updateGun() {
        weaponScript = player.GetComponentInChildren<Weapon>();
    }

    private void focusInput() {
        inputField.GetComponent<InputField>().Select();
        inputField.GetComponent<InputField>().ActivateInputField();
    }
    
    //Commands with no privilege
	public static void printLimbo() {
		print("Limbo");
		if (!Pooling.Empty())
			print(Pooling.getLimbo());
		else
			print("Limbo is empty.");
	}

    public static void printByName(string name) {
        GameObject obj = GameObject.Find(name);
        print("GameObject info");
        print("Name: " + obj.name);
        print("Position: " + obj.transform.position);
        print("Rotation: " + obj.transform.eulerAngles);
    }

    public static void printByTag(string name) {
        GameObject[] list = GameObject.FindGameObjectsWithTag(name);
        foreach (GameObject entity in list)
            print(entity.name);
    }

    public void sensitivity(float value) {
        SettingsManager.SetParameter("MouseSlider", value);
	}

    public void showFPS() {
        fps.GetComponent<Image>().enabled = true;
        fps.GetComponentInChildren<Text>().enabled = true;
        fps.GetComponent<FPSCounter>().enabled = true;
        fps.GetComponent<FPSDisplay>().enabled = true;
    }

    public void hideFPS() {
        fps.GetComponent<FPSDisplay>().enabled = false;
        fps.GetComponent<FPSCounter>().enabled = false;
        fps.GetComponent<Image>().enabled = false;
        fps.GetComponentInChildren<Text>().enabled = false;
    }

    //TODO: all these commands will need an admin privilege

    public static void destroyByTag(string tag) {
        GameObject[] objectsWithTag = GameObject.FindGameObjectsWithTag(tag);
		foreach (GameObject obj in objectsWithTag) {
            if (Application.isPlaying)
			    Destroy(obj);
            else
                DestroyImmediate(obj);
        }
    }

    public static void setPosition(string name, Vector3 pos) {
        GameObject obj = GameObject.Find(name);
        obj.transform.position = pos;
        print(obj.name + " position changed!");
    }

    public static void setEulerAngles(string name, Vector3 angles) {
        GameObject obj = GameObject.Find(name);
        angles = new Vector3(Mathf.Repeat(angles.x, 360), 
                             Mathf.Repeat(angles.y, 360),
                             Mathf.Repeat(angles.z, 360));
        obj.transform.localRotation = Quaternion.Euler(angles);
        print(obj.name + " rotation changed!");
    }
}
