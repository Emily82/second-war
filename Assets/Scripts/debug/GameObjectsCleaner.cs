﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Usefull debug tool to remove ghost gameobjects in the scene (Unity bug)
[ExecuteInEditMode]
public class GameObjectsCleaner : MonoBehaviour {
	[Tooltip("All gameobjects tagged with this value will be removed")]
	public string Tag;

	void OnEnable () {
		if (Tag != null) {
			Console.destroyByTag(Tag);
		}
		else
			print("GameObjectsCleaner: cannot remove any gameobject, no tag value");
	}
}
