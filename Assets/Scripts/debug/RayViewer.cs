﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//RayCast debugger
public class RayViewer : MonoBehaviour {
	public float weaponRange = 1000;
	private Camera fpsCam;

	// Use this for initialization
	void Start () {
		fpsCam = transform.parent.Find("Camera").GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 lineOrigin = fpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
		//Visible onnly in Scene view not in game
		Debug.DrawRay(lineOrigin, fpsCam.transform.forward * weaponRange, Color.green);
	}
}
