﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attached to "TestBox" gameobject
public class ShootableBox : Shootable {

	public override void Damage(float amount) {
		//subtract damage amount when Damage function is called
		currentHealth -= amount;

		if (currentHealth <= 0)
			rootEntityStatus.Die();
	}
}
