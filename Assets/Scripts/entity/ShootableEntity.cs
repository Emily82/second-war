﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attached to "Body" or "BodyBot" gameobjects
public class ShootableEntity : Shootable {
	public override void Damage(float amount) {
		//subtract damage amount when Damage function is called
		currentHealth -= amount;

		if (currentHealth <= 0)
			rootEntityStatus.Die();
	}

	//Used by console command
	public void suicide() {
		Damage(currentHealth + 1);
	}
}
