﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attached to "Head" and all boxes treated as hurtboxes
public class Hurtbox : MonoBehaviour {
	[Tooltip("Percentage of gun damage for this hurtbox")]
	//Default 100% of received damage (example: head hurtbox)
	//If this is an arm hurtbox the gun damage received will be reduced (ex. 50%)
	public float damagePercentage = 100;

	private Shootable me;
	private GameObject rootGameObject;
	private EntityStatus entityStatus;

	// Use this for initialization
	void Start () {
		//NOTE: GetComponentInParent start to search from this object too
		me = GetComponentInParent<Shootable>();
		entityStatus = GetComponentInParent<EntityStatus>();
		rootGameObject = entityStatus.gameObject;
	}

	public void Damage(float amount, string offenderName, bool selfDamage = false) {
		//No damage on self shooting. If someone shoots me then damage only if GodMode is false
		if (selfDamage || (entityStatus.IsAlive && offenderName != rootGameObject.name && !entityStatus.GodMode)) {
			float hurtboxDamage = amount * (damagePercentage / 100);
			print("Hurtbox: " + gameObject.name + " " + hurtboxDamage);
			//Call Shootable class Damage() to update health status
			me.Damage(hurtboxDamage);
		}
		else {
			if (entityStatus.GodMode)
				print("Hurtbox: GodMode enabled, no damage!");
			else
				print("Hurtbox " + gameObject.name + ": self damage not applied.");
		}
	}
}
