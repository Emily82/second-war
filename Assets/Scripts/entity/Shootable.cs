using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class will not be attached to any gameobject
public abstract class Shootable : MonoBehaviour {
    public float currentHealth = 100;
    public abstract void Damage(float amount);
    public AudioClip damageClip;

    protected GameObject rootGameObject;
    protected EntityStatus rootEntityStatus;
    protected float defaultHealth;

    void Awake() {
        defaultHealth = currentHealth;
    }

    public void RestoreHealth() {
		currentHealth = defaultHealth;
	}

    //Start will be inherited by all child classes
    void Start() {
        //Getting the Player / Bot root entity gameobject
        rootEntityStatus = GetComponentInParent<EntityStatus>();
        rootGameObject = rootEntityStatus.gameObject;
    }

    //TODO: implement the coroutine that will play damage clip (called by damage function in child classes)
    /* protected IEnumerator DamageEffect() {
    } */
}