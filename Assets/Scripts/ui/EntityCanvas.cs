﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityCanvas : MonoBehaviour {

	private Transform target;
	private GameObject godModeIcon;

	void Start () {
		//Better to use player transform as target than Camera
		target = GameObject.FindGameObjectWithTag("Player").transform;
		godModeIcon = transform.Find("Godmode").gameObject;
	}

	// Update is called once per frame
	void Update () {
		//We have to make sure the canvas looks at the main camera
		//For performance its better not to call lookAt when the icon is disabled
		if (target && godModeIcon.activeSelf) {
			transform.LookAt(target);
		}
	}
}
