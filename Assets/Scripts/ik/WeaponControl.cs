﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))] 
//Set IK constraints to weapon grabHandles for both right and left hands
public class WeaponControl : MonoBehaviour {
    
    public bool ikActive = false;

    private Animator animator;
    private Transform leftGrabHandleObj = null;
    private Transform rightGrabHandleObj = null;
    private Transform lookObj = null;
    private Transform leftElbowHint;
    private Weapon gun;
    private FingersPose fingersPose;
    private bool elbowIKHintEnabled = true;

    void Start ()
    {
        animator = GetComponent<Animator>();
        leftElbowHint = transform.Find("LeftElbowHint");
        updateGun();
    }

    public void updateGun() {
        gun = GetComponentInChildren<Weapon>();
        fingersPose = gun.GetComponent<FingersPose>();
    }

    public void setGrabHandles(Transform LeftGrabHandleObj, Transform RightGrabHandleObj, Transform LookObj) {
        //At least one hand must be specified
        if ((RightGrabHandleObj || LeftGrabHandleObj) && LookObj) {
            this.leftGrabHandleObj = LeftGrabHandleObj;
            this.rightGrabHandleObj = RightGrabHandleObj;
            this.lookObj = LookObj;
        }
        else
            print("WeaponControl: warning! Not enough grab handles set for inverse kinematics!");
    }

    public void enableElbowAdjust() {
        elbowIKHintEnabled = true;
    }

    public void disableElbowAdjust() {
        elbowIKHintEnabled = false;
    }
    
    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if(animator) {
            //if the IK is active, set the position and rotation directly to the goal. 
            if(ikActive) {

                // Set the look target position, if one has been assigned
                if(lookObj != null) {
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookObj.position);
                }    

                // Set the right hand target position and rotation, if one has been assigned
                if(rightGrabHandleObj != null) {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);  
                    animator.SetIKPosition(AvatarIKGoal.RightHand, rightGrabHandleObj.position);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, rightGrabHandleObj.rotation);
                }    
                if (leftGrabHandleObj != null) {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);  
                    animator.SetIKPosition(AvatarIKGoal.LeftHand, leftGrabHandleObj.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, leftGrabHandleObj.rotation);
                }    
                animator.stabilizeFeet = true;
                if (elbowIKHintEnabled)
                    adjustElbows();
                adjustFingers();
            }
            
            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else {          
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0); 
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }

    //Adjust elbows rotation
    private void adjustElbows() {
        if (leftElbowHint) {
            //Left elbow
            animator.SetIKHintPosition(AvatarIKHint.RightElbow, leftElbowHint.position);
            animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, 1);
            //TODO: right elbow
        }
    }

    //Adjust fingers rotation using data from FingersPose script (in weapon)
    private void adjustFingers() {
		if (fingersPose && fingersPose.enableAdjust) {
            //Right fingers proximal falanges
            Helper.adjustBone(animator, HumanBodyBones.RightThumbProximal, fingersPose.rightThumb1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightThumbProximal, fingersPose.rightThumb1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightThumbProximal, fingersPose.rightThumb1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexProximal, fingersPose.rightIndex1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexProximal, fingersPose.rightIndex1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexProximal, fingersPose.rightIndex1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleProximal, fingersPose.rightMiddle1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleProximal, fingersPose.rightMiddle1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleProximal, fingersPose.rightMiddle1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightRingProximal, fingersPose.rightRing1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightRingProximal, fingersPose.rightRing1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightRingProximal, fingersPose.rightRing1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleProximal, fingersPose.rightLittle1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleProximal, fingersPose.rightLittle1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleProximal, fingersPose.rightLittle1Yaw, EulerAngle.Yaw);
            //Right fingers intermediate falanges
            Helper.adjustBone(animator, HumanBodyBones.RightThumbIntermediate, fingersPose.rightThumb2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightThumbIntermediate, fingersPose.rightThumb2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightThumbIntermediate, fingersPose.rightThumb2Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexIntermediate, fingersPose.rightIndex2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexIntermediate, fingersPose.rightIndex2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexIntermediate, fingersPose.rightIndex2Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleIntermediate, fingersPose.rightMiddle2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleIntermediate, fingersPose.rightMiddle2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleIntermediate, fingersPose.rightMiddle2Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightRingIntermediate, fingersPose.rightRing2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightRingIntermediate, fingersPose.rightRing2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightRingIntermediate, fingersPose.rightRing2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleIntermediate, fingersPose.rightLittle2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleIntermediate, fingersPose.rightLittle2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleIntermediate, fingersPose.rightLittle2Yaw, EulerAngle.Yaw);
            //Right fingers distal falanges
            Helper.adjustBone(animator, HumanBodyBones.RightThumbDistal, fingersPose.rightThumb3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightThumbDistal, fingersPose.rightThumb3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightThumbDistal, fingersPose.rightThumb3Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexDistal, fingersPose.rightIndex3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexDistal, fingersPose.rightIndex3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightIndexDistal, fingersPose.rightIndex3Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleDistal, fingersPose.rightMiddle3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleDistal, fingersPose.rightMiddle3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightMiddleDistal, fingersPose.rightMiddle3Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.RightRingDistal, fingersPose.rightRing3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightRingDistal, fingersPose.rightRing3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightRingDistal, fingersPose.rightRing3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleDistal, fingersPose.rightLittle3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleDistal, fingersPose.rightLittle3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.RightLittleDistal, fingersPose.rightLittle3Yaw, EulerAngle.Yaw);
            //Left Fingers proximal falanges
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbProximal, fingersPose.leftThumb1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbProximal, fingersPose.leftThumb1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbProximal, fingersPose.leftThumb1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexProximal, fingersPose.leftIndex1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexProximal, fingersPose.leftIndex1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexProximal, fingersPose.leftIndex1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleProximal, fingersPose.leftMiddle1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleProximal, fingersPose.leftMiddle1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleProximal, fingersPose.leftMiddle1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingProximal, fingersPose.leftRing1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingProximal, fingersPose.leftRing1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingProximal, fingersPose.leftRing1Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleProximal, fingersPose.leftLittle1Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleProximal, fingersPose.leftLittle1Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleProximal, fingersPose.leftLittle1Roll, EulerAngle.Roll);
            //Left fingers intermediate falanges
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbIntermediate, fingersPose.leftThumb2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbIntermediate, fingersPose.leftThumb2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbIntermediate, fingersPose.leftThumb2Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexIntermediate, fingersPose.leftIndex2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexIntermediate, fingersPose.leftIndex2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexIntermediate, fingersPose.leftIndex2Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleIntermediate, fingersPose.leftMiddle2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleIntermediate, fingersPose.leftMiddle2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleIntermediate, fingersPose.leftMiddle2Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingIntermediate, fingersPose.leftRing2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingIntermediate, fingersPose.leftRing2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingIntermediate, fingersPose.leftRing2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleIntermediate, fingersPose.leftLittle2Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleIntermediate, fingersPose.leftLittle2Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleIntermediate, fingersPose.leftLittle2Yaw, EulerAngle.Yaw);
            //Left fingers distal falanges
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbDistal, fingersPose.leftThumb3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbDistal, fingersPose.leftThumb3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftThumbDistal, fingersPose.leftThumb3Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexDistal, fingersPose.leftIndex3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexDistal, fingersPose.leftIndex3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftIndexDistal, fingersPose.leftIndex3Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleDistal, fingersPose.leftMiddle3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleDistal, fingersPose.leftMiddle3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftMiddleDistal, fingersPose.leftMiddle3Roll, EulerAngle.Roll);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingDistal, fingersPose.leftRing3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingDistal, fingersPose.leftRing3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftRingDistal, fingersPose.leftRing3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleDistal, fingersPose.leftLittle3Pitch, EulerAngle.Pitch);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleDistal, fingersPose.leftLittle3Yaw, EulerAngle.Yaw);
            Helper.adjustBone(animator, HumanBodyBones.LeftLittleDistal, fingersPose.leftLittle3Yaw, EulerAngle.Yaw);
		}
	}
}
