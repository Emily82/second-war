using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintableDictionary<Tkey, Tvalue> : Dictionary<Tkey, Tvalue> {
	public override string ToString() {
		string output = "";
		foreach(Tkey k in this.Keys) {
			output += k + ": " + this[k] + "\n";
		}
		return output;
	}
}
