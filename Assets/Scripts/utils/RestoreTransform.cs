﻿using UnityEngine;

//Restore to default position and rotation any game object using this script
public class RestoreTransform : MonoBehaviour {
	private Vector3 defaultPos;
	private Quaternion defaultRot;

	void Awake() {
		//Save default transform
		defaultPos = new Vector3(transform.localPosition.x,
								 transform.localPosition.y,
								 transform.localPosition.z);
		defaultRot = new Quaternion(transform.localRotation.x,
									transform.localRotation.y,
									transform.localRotation.z,
									transform.localRotation.w);
	}

	public void Restore() {
		RestorePos();
		RestoreRot();
	}

	public void RestorePos() {
		transform.localPosition = defaultPos;
	}

	public void RestoreRot() {
		transform.localRotation = defaultRot;
	}
}
