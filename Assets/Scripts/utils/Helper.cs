﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EulerAngle {
    Pitch,
    Yaw,
    Roll
}

public class Helper {
	//0 : dont collide
	//1 : collide
	//Example
	//Ignore the layer number 2 (Ignore Raycast) and 8 (WeaponsLayer)
	//Layer Mask: 011111011
	public static int getLayerMask(params string[] layers) {
		return ~(LayerMask.GetMask(layers));
	}

    //Toggle animator parameter state. Parameter must be bool in animator
    public static void toggleParam(Animator animator, string paramName, bool chkErr = true) 
    {
        //Slower version with error checking
        if (chkErr) {
            bool oldState;
            if (paramName.Length > 0) {
                oldState = animator.GetBool(paramName);
                if (animator)
                    animator.SetBool(paramName, !oldState);
                else
                    Debug.LogWarning("Helper.toggleParam(): cannot toggle parameter, animator is null");

                //Checking if toogle worked otherwise write warnings
                bool newState = animator.GetBool(paramName);
                if (oldState == newState)
                    Debug.LogWarning("Helper.toggleParam(): parameter" + paramName + "does not exist!");
            }
            else
                Debug.LogWarning("Helper.toggleParam(): parameter name is empty!");
        }
        //Fast version but no error checking
        else
            animator.SetBool(paramName, !animator.GetBool(paramName));
    }

	//NOTE: these method WORKS ONLY if called from OnAnimatorIK() context

	//Adjust bone local rotation specified by angle degrees with type euler angle.
	//Other euler angles will not be changed
    public static void adjustBone(Animator animator, HumanBodyBones humanBone, float angle, EulerAngle type) {
        if (angle != 0) {
            Transform boneTransform = animator.GetBoneTransform(humanBone);
            //if current model doesn't have this bone, simply ignore the adjust
            if (boneTransform != null) {
                switch(type) {
                    case (EulerAngle.Pitch):
                            boneTransform.localRotation = Quaternion.Euler(boneTransform.localEulerAngles.x + angle,
                                                                           boneTransform.localEulerAngles.y,
                                                                           boneTransform.localEulerAngles.z);
                    break;
                    case (EulerAngle.Yaw):
                            boneTransform.localRotation = Quaternion.Euler(boneTransform.localEulerAngles.x,
                                                                           boneTransform.localEulerAngles.y + angle,
                                                                           boneTransform.localEulerAngles.z);
                    break;
                    case (EulerAngle.Roll):
                            boneTransform.localRotation = Quaternion.Euler(boneTransform.localEulerAngles.x,
                                                                           boneTransform.localEulerAngles.y,
                                                                           boneTransform.localEulerAngles.z + angle);
                    break;
                }
                if (animator)
                    animator.SetBoneLocalRotation(humanBone, boneTransform.localRotation);
                else
                    Debug.LogWarning("Helper: cannot adjust bone, animator is null");
            }
        }
    }

	//Set bone local rotation to angle degrees of euler angle type.
	//Other euler angles will not be changed
	public static void rotateBone(Animator animator, HumanBodyBones humanBone, float angle, EulerAngle type) {
        if (angle != 0) {
            Transform boneTransform = animator.GetBoneTransform(humanBone);
            //if current model doesn't have this bone, simply ignore the adjust
            if (boneTransform != null) {
                switch(type) {
                    case (EulerAngle.Pitch):
                            boneTransform.localRotation = Quaternion.Euler(angle,
                                                                           boneTransform.localEulerAngles.y,
                                                                           boneTransform.localEulerAngles.z);
                    break;
                    case (EulerAngle.Yaw):
                            boneTransform.localRotation = Quaternion.Euler(boneTransform.localEulerAngles.x,
                                                                           angle,
                                                                           boneTransform.localEulerAngles.z);
                    break;
                    case (EulerAngle.Roll):
                            boneTransform.localRotation = Quaternion.Euler(boneTransform.localEulerAngles.x,
                                                                           boneTransform.localEulerAngles.y,
                                                                           angle);
                    break;
                }
                if (animator)
                    animator.SetBoneLocalRotation(humanBone, boneTransform.localRotation);
                else
                    Debug.LogWarning("Helper: cannot rotate bone, animator is null");
            }
        }
    }

    //Handle mouse cursor state
    public static void showCursor() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public static void hideCursor() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    //Disable/Enable/Toggle user input scripts
    public static void disableUserInput(Weapon weaponScript, Movement movementScript, MouseCamera mouseCameraScript) {
        weaponScript.enabled = false;
        movementScript.enabled = false;
        mouseCameraScript.enabled = false;
    }

    public static void enableUserInput(Weapon weaponScript, Movement movementScript, MouseCamera mouseCameraScript) {
        weaponScript.enabled = true;
        movementScript.enabled = true;
        mouseCameraScript.enabled = true;
    }

    public static void toggleUserInput(Weapon weaponScript, Movement movementScript, MouseCamera mouseCameraScript) {
        weaponScript.enabled = !weaponScript.enabled;
        movementScript.enabled = !movementScript.enabled;
        mouseCameraScript.enabled = !mouseCameraScript.enabled;
    }
}
