using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//In this class we can keep relation between entity game object and the prefab used to create it
public class Entity {
    private MapLoader mapLoader;
    private GameObject rootGameObject;
    private GameObject prefabUsed;

    public GameObject RootGameObject {
        get {
            return rootGameObject;
        }
    }

    public GameObject PrefabUsed {
        get {
            return prefabUsed;
        }
    }

    public Entity(string name, GameObject prefabUsed, Transform parent) {
        mapLoader = MapLoader.instance;
		if (prefabUsed != null) {
            EntityStatus entityStatus;
            this.prefabUsed = prefabUsed;
			this.rootGameObject = Pooling.RecycleGameObject(prefabUsed, parent);
			this.rootGameObject.name = name;
            entityStatus = rootGameObject.GetComponent<EntityStatus>();
			this.rootGameObject.transform.position = mapLoader.getNewPosition(entityStatus.team);
		}
    }

    public void TrashEntity() {
        //We avoid calling this method even when this entity its already in trash
        if (!isTrashed()) {
            rootGameObject.name = prefabUsed.name;
            Pooling.TrashGameObject(rootGameObject);
        }
        else
            Debug.LogWarning("Entity: Warning trying to trash an already trashed entity.");
    }

    private bool isTrashed() {
        return Pooling.isTrashed(rootGameObject);
    }
}
