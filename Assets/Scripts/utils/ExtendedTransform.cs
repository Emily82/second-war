﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtendedTransform {
	 public static Transform recursiveFind(this Transform parent, string name)
     {
         var result = parent.Find(name);
         if (result != null)
             return result;
         foreach(Transform child in parent)
         {
             result = child.recursiveFind(name);
             if (result != null)
                 return result;
         }
         return null;
     }
}
