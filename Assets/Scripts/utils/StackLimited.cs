using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Custom version of Stack with max capacity
public class StackLimited<T> : Stack<T> {
    private int capacity;

    public StackLimited(int capacity) {
        this.capacity = capacity;
    }

    //true if pushed item, false if stack full
	public new bool Push (T item) {
        if (Full())
            return false;
        else {
            base.Push(item);
            return true;
        }
    }

    public bool Empty() {
        return this.Count <= 0;
    }

    public bool Full() {
        return this.Count >= this.capacity;
    }

    public override string ToString() {
        string output = base.ToString();
        //Number of items for this stack
		output += " #" + Count;
		return output;
    }
}