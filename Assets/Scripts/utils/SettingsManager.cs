﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using KeyMap = PrintableDictionary<string, UnityEngine.KeyCode[]>;
using AxisValues = PrintableDictionary<string, float[]>;

//In this class we store all default input settings and define methods to customize and retrieve them
public static class SettingsManager {
    const int smoothTime = 16;

    public static float mouseSensitivity {get;private set;}
    static KeyMap buttonMapping;
    static KeyMap axisMapping;
    static readonly string[] axisNames = new string[] {
        "Horizontal",
        "Vertical",
        "Mouse ScrollWheel"
    };
    static readonly KeyCode[][] defaultAxisBinds = new KeyCode[][] {
        new KeyCode[] {         //Horizontal
            KeyCode.RightArrow, //Positive button
            KeyCode.LeftArrow,  //Negative button
            KeyCode.D,          //Alt Positive
            KeyCode.A           //Alt Negative
        },
        new KeyCode[] {         //Vertical
            KeyCode.UpArrow,
            KeyCode.DownArrow,
            KeyCode.W,
            KeyCode.S
        }

    };
    static AxisValues current; //key -> [Positive value, Negative value]
    static AxisValues velocity;

    static readonly string[] buttonNames = new string[] {
        "Fire1",
        "Fire2",
        "Fire3",
        "Jump",
        "Submit",
        "Cancel",
        "Crouch",
        "PrimaryWeapon",
        "SecondaryWeapon",
        "Grenade",
        "LeanLeft",
        "LeanRight",
        "Console"
    };
    static readonly KeyCode[][] defaultButtonBinds = new KeyCode[][] {
        new KeyCode[] {             //Fire1
            KeyCode.Mouse0,         //Button
            KeyCode.LeftControl     //Alternative button
        },
        new KeyCode[] {             //Fire2
            KeyCode.Mouse1,         
            KeyCode.LeftAlt         
        },
        new KeyCode[] {             //Fire3
            KeyCode.Mouse2,         
            KeyCode.R               
        },
        new KeyCode[] {             //Jump         
            KeyCode.Space             
        },
        new KeyCode[] {             //Submit         
            KeyCode.F,
            KeyCode.Return       
        },
        new KeyCode[] {             //Cancel         
            KeyCode.Escape            
        },
        new KeyCode[] {             //Crouch         
            KeyCode.C          
        },
        new KeyCode[] {             //PrimaryWeapon         
            KeyCode.Alpha1           
        },
        new KeyCode[] {             //SecondaryWeapon         
            KeyCode.Alpha2     
        },
        new KeyCode[] {             //Grenade         
            KeyCode.Alpha3     
        },
        new KeyCode[] {             //LeanLeft         
            KeyCode.Q      
        },
        new KeyCode[] {             //LeanRight         
            KeyCode.E               
        },
        new KeyCode[] {             //Console         
            KeyCode.Backslash,
            KeyCode.Less           
        }
    };

    public static void Initialize(bool initParams = true) {
        if (initParams) {
            //Init general parameters like mouse sensitivity, audio settings, etc ...
            InitParameter("MouseSlider");
            InitParameter("AudioSlider");
        }

        current = new AxisValues();
        velocity = new AxisValues();
        foreach(string axisName in axisNames) {
            //Init all axis values
            current[axisName] = new float[] {0, 0};
            velocity[axisName] = new float[] {0, 0};
        }

        if (axisMapping == null) {
            axisMapping = new KeyMap();
            //NOTE: We need to exclude "Mouse ScrollWheel" axis, it will be not handled using keys
            for (int i = 0 ; i < axisNames.Length - 1 ; ++i) {
                string axisName = axisNames[i];
                //Retrieve data from PlayerPref if key is not null
                if (PlayerPrefs.HasKey(axisName))
                    axisMapping[axisName] = toKeyCodeArray(PlayerPrefsX.GetStringArray(axisName));
                else {
                //Otherwise set default hardcoded data and save all to PlayerPref
                    axisMapping[axisName] = new KeyCode[defaultAxisBinds[i].Length];
                    defaultAxisBinds[i].CopyTo(axisMapping[axisName], 0);
                    PlayerPrefsX.SetStringArray(axisName, toStringArray(defaultAxisBinds[i]));
                }
            }
        }
        if (buttonMapping == null) {
            buttonMapping = new KeyMap();
            for (int i = 0 ; i < buttonNames.Length ; ++i) {
                string buttonName = buttonNames[i];
                //Retrieve data from PlayerPref if key is not null
                if (PlayerPrefs.HasKey(buttonName))
                    buttonMapping[buttonName] = toKeyCodeArray(PlayerPrefsX.GetStringArray(buttonName));
                else {
                //Otherwise set default hardcoded data and save all to PlayerPref
                    buttonMapping[buttonName] = new KeyCode[defaultButtonBinds[i].Length];
                    defaultButtonBinds[i].CopyTo(buttonMapping[buttonName], 0);
                    PlayerPrefsX.SetStringArray(buttonName, toStringArray(defaultButtonBinds[i]));
                }
            }
        }
    }

    public static void InitParameter(string name) {
        if (!PlayerPrefs.HasKey(name))
            SetParameter(name, 1);
        else
            SetParameter(name, PlayerPrefs.GetFloat(name));
    }

    //Reset all buttons / axis settings in user preferences to default values
    public static void ResetKeyPlayerPrefs() {
        //Delete all keys
        foreach (string name in axisNames) {
            if (PlayerPrefs.HasKey(name))
                PlayerPrefs.DeleteKey(name);
        }
        foreach (string name in buttonNames) {
            if (PlayerPrefs.HasKey(name))
                PlayerPrefs.DeleteKey(name);
        }
        axisMapping.Clear();
        buttonMapping.Clear();
        axisMapping = null;
        buttonMapping = null;
        Initialize(false);
    }

    public static void SetParameter(string name, float value) {
        if (PlayerPrefs.HasKey(name)){
            PlayerPrefs.SetFloat(name, value);
            switch (name) {
                case "MouseSlider":
                    mouseSensitivity = value;
                break;
                case "AudioSlider":
                    AudioListener.volume = value;
                break;
            }
        }
    }

    public static KeyCode[] GetBinds(string name) {
        if (axisMapping.ContainsKey(name))
            return axisMapping[name];
        else if (buttonMapping.ContainsKey(name))
            return buttonMapping[name];
        else
            return null;
    }

    public static void SetBindArray(string name, KeyCode[] binds) {
        KeyMap mapping = null;

        if (axisMapping.ContainsKey(name))
            mapping = axisMapping;
        else if (buttonMapping.ContainsKey(name))
            mapping = buttonMapping;
        else {
            Debug.LogWarning("SettingsManager: cannot set bind array, '" + name + "' key does not exist.");
            return;
        }

        Array.Copy(binds, mapping[name], binds.Length);
        PlayerPrefs.DeleteKey(name);
        PlayerPrefsX.SetStringArray(name, toStringArray(binds));
    }

    public static void SetBind(string name, KeyCode bind, int index) {
        KeyMap mapping = null;

        if (axisMapping.ContainsKey(name))
            mapping = axisMapping;
        else if (buttonMapping.ContainsKey(name))
            mapping = buttonMapping;
        else {
            Debug.LogWarning("SettingsManager: cannot set bind  '" + name + "' key does not exist.");
            return;
        }

        mapping[name][index] = bind;
        PlayerPrefsX.SetStringArray(name, toStringArray(mapping[name]));
    }

    public static bool GetButtonUp(string buttonName) {
        bool status = false;
        if (buttonMapping.ContainsKey(buttonName)) {
            foreach(KeyCode button in buttonMapping[buttonName]) {
                if (Input.GetKeyUp(button)) {
                    status = true;
                    break;
                }
            }
        }
        return status;
    }

    public static bool GetButtonDown(string buttonName) {
        bool status = false;
        if (buttonMapping.ContainsKey(buttonName)) {
            foreach(KeyCode button in buttonMapping[buttonName]) {
                if (Input.GetKeyDown(button)) {
                    status = true;
                    break;
                }
            }
        }
        return status;
    }

    public static bool GetButton(string buttonName) {
        bool status = false;
        if (buttonMapping.ContainsKey(buttonName)) {
            foreach(KeyCode button in buttonMapping[buttonName]) {
                if (Input.GetKey(button)) {
                    status = true;
                    break;
                }
            }
        }
        return status;
    }

    public static float GetAxis(string axisName) {
        //For mouse wheel we need to use Unity GetAxis
        if (axisName == "Mouse ScrollWheel")
            return Input.GetAxis("Mouse ScrollWheel");

        KeyCode[] axisBinds = axisMapping[axisName];
        int i = 0;
        float axisValue = 0;

        foreach(KeyCode bind in axisBinds) {
            if (Input.GetKey(bind)) {
                //Positive axis for even index, Negative for odd
                if ((i % 2) == 0) {
                    current[axisName][0] = axisValue = Mathf.SmoothDamp(current[axisName][0], 
                                                                        1, 
                                                                        ref velocity[axisName][0], 
                                                                        Time.deltaTime * smoothTime);
                }
                else {
                    current[axisName][1] = axisValue = Mathf.SmoothDamp(current[axisName][1], 
                                                                        -1, 
                                                                        ref velocity[axisName][1], 
                                                                        Time.deltaTime * smoothTime);
                }
            }
            else if (Input.GetKeyUp(bind))
                current[axisName][0] = current[axisName][1] = 0;
            ++i;
        }
        return axisValue;
    }

    //Convert array of string into array of keycode
    private static KeyCode[] toKeyCodeArray(string[] binds) {
        List<KeyCode> convertedBinds = new List<KeyCode>();

        foreach(string bind in binds)
            convertedBinds.Add((KeyCode)System.Enum.Parse(typeof(KeyCode), bind));

        return convertedBinds.ToArray();
    }
    //Convert array of keycode into array of string
    private static string[] toStringArray(KeyCode[] binds) {
        List<string> convertedBinds = new List<string>();

        foreach(KeyCode bind in binds)
            convertedBinds.Add(bind.ToString());

        return convertedBinds.ToArray();
    }
}
