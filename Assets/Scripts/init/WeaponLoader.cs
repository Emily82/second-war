﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attached to "Weapon" gameobject
public class WeaponLoader : MonoBehaviour {
	public GameObject prefabPrimaryWeapon;
	public GameObject prefabSecondaryWeapon;
	public GameObject prefabGrenadeWeapon;

	//To have nice indexes for array of bools cartridgeToRestore
	private enum WeaponType {
		primary,
		secondary,
		grenade
	}
	//This array tells us if it is needed cartridge restore when weapon is selected
	private bool[] cartridgeToRestore = {false, false, false};
	private GameObject currentWeapon = null;
	private MouseCamera mouseCamera;
	private EntityStatus entityStatus;
	private WeaponControl weaponControl;
	private Console console;
	private GameObject currentOwner;

	void Awake() {
		mouseCamera = GetComponentInParent<MouseCamera>();
		entityStatus = GetComponentInParent<EntityStatus>();
		weaponControl = GetComponentInParent<WeaponControl>();
		console = Console.instance;
		//In the beginning there is no need to update other scripts references to this gun
		if (prefabPrimaryWeapon)
			setCurrentWeapon(prefabPrimaryWeapon);
	}

	void Start() {
		currentOwner = mouseCamera.gameObject;
	}

	//This will set flags to restore cartridge
	public void restoreAllCartridges() {
		for (int i = 0 ; i < cartridgeToRestore.Length ; ++i)
			cartridgeToRestore[i] = true;
	}

	//Called by console (sets prefab weapon and not currentWeapon)
	public void setPrimaryPrefabWithName(string name) {
		if (name != null && name.Length > 0) {
			int codeErr = setPrimaryPrefab((GameObject) Resources.Load(GeneralParameters.weaponsPrefabPath + name));
			if (codeErr == 0)
				print(prefabPrimaryWeapon.name + " weapon prefab Loaded");
			else
				print(name + " weapon prefab does not exist!");
		}
	}

	//This will set weapon for next respawn (returns 1 if prefabPrimaryWeapon is null)
	public int setPrimaryPrefab(GameObject prefabPrimaryWeapon) {
		int error = 0;
		if (prefabPrimaryWeapon != null)
			this.prefabPrimaryWeapon = prefabPrimaryWeapon;
		else
			error = 1;
		return error;
	}

	//Loads primary weapon
	public void SelectPrimaryWeapon() {	
		if (prefabPrimaryWeapon == null)
			Debug.LogWarning("WeaponLoader: no primary prefab weapon!");
		else {
			if (weaponChanged()) {
				setCurrentWeapon(prefabPrimaryWeapon);
				//Update this gun reference for all scripts that need it
				UpdateGunReferences();
				UpdateCurrentWeaponOwner();
			}
			if (cartridgeToRestore[(int)WeaponType.primary]) {
				cartridgeToRestore[(int)WeaponType.primary] = false;
				currentWeapon.GetComponent<Weapon>().RestoreCartridge();
			}
		}
	}

	public void SelectSecondaryWeapon() {
		if (prefabSecondaryWeapon) {
			setCurrentWeapon(prefabSecondaryWeapon);
			//Update this gun reference for all scripts that need it
			UpdateGunReferences();
			UpdateCurrentWeaponOwner();
			if (cartridgeToRestore[(int)WeaponType.secondary]) {
				cartridgeToRestore[(int)WeaponType.secondary] = false;
				currentWeapon.GetComponent<Weapon>().RestoreCartridge();
			}
		}
	}

	public void SelectGrenadeWeapon() {
		if (prefabGrenadeWeapon) {
			setCurrentWeapon(prefabGrenadeWeapon);
			//Update this gun reference for all scripts that need it
			UpdateGunReferences();
			UpdateCurrentWeaponOwner();
			if (cartridgeToRestore[(int)WeaponType.grenade]) {
				cartridgeToRestore[(int)WeaponType.grenade] = false;
				currentWeapon.GetComponent<Weapon>().RestoreCartridge();
			}
		}
	}

	public void UpdateGunReferences() {
		if (mouseCamera && entityStatus && weaponControl) {
			mouseCamera.updateGun();
			entityStatus.updateGun();
			weaponControl.updateGun();
			console.updateGun();
		}
		else
			Debug.LogWarning("WeaponLoader: missing reference to entity scripts to update gun!");
	}

	public void UpdateCurrentWeaponOwner() {
		Weapon weapon = currentWeapon.GetComponent<Weapon>();
		//optimization to avoid update all when new owner its the same of the old one
		if (weapon.Owner != currentOwner) {
			//Update new gun entity owner
			currentWeapon.GetComponent<Weapon>().updateOwner();
		}
	}

	//Change current weapon
	private void setCurrentWeapon(GameObject prefabWeapon) {
		//Trash out old weapon
		Pooling.TrashGameObject(currentWeapon);

		//Spawn the gun (example Karabiner98)
		//NOTE: RecycleGameObject can instantiate if pool is empty.
		currentWeapon = Pooling.RecycleGameObject(prefabWeapon, transform);
		currentWeapon.name = prefabWeapon.name;
	
		//Set weapon constraints for the IK script WeaponControl
		Transform LeftGrabHandle = currentWeapon.transform.Find("LeftGrabHandle");
		Transform RightGrabHandle = currentWeapon.transform.Find("RightGrabHandle");
		Transform LookObj = currentWeapon.transform.Find("GunEnd");
		weaponControl.setGrabHandles(LeftGrabHandle, RightGrabHandle, LookObj);
	}

	private bool weaponChanged() {
		return ((currentWeapon == null && prefabPrimaryWeapon != null) || 
				(prefabPrimaryWeapon.name != currentWeapon.name));
	}
}
