﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PoolingTable = PrintableDictionary< string, StackLimited<UnityEngine.GameObject> >;

//Attached in runtime by MapLoader in Main game object
public class Pooling : MonoBehaviour {
	public static Pooling instance;

	[SerializeField]
	[Tooltip("Maximum capacity for each pooling stack.")]
	private int Capacity = 20;

	private static int stackCapacity; //So it can be accessed from static methods
	private static PoolingTable Limbo;

	void Awake() {
		instance = this;
		stackCapacity = Capacity;
	}

	//Called by MapLoader
	public void init() {
		Limbo = new PoolingTable();
		//Add all gameobjects preloaded in trashcan
		GameObject TrashCan = GameObject.FindGameObjectWithTag("TrashCan");
		Transform[] Children = TrashCan.GetComponentsInChildren<Transform>(true);
		//Lets register in pooling trashcan manual preloaded game objects
		//NOTE: remember that trashcan should contain only INACTIVE gameobjects
		foreach(Transform child in Children) {
			//Filter nested levels
			if (child.parent.gameObject == TrashCan) {
				if (!Limbo.ContainsKey(child.name))
					Limbo.Add(child.name, new StackLimited<GameObject>(stackCapacity));

				//If too many game object are preloaded in trashcan they will be destroyed
				//NOTE: however from Unity Editor, it should be wise to avoid
				// 		manually preloading too many objects in trashcan
				if (!Limbo[child.name].Push(child.gameObject))
					Destroy(child.gameObject);
			}
		}
	}

	public static PoolingTable getLimbo() {
		return Limbo;
	}

	public static bool Empty() {
		return (Limbo.Count <= 0);
	}

	public static bool isTrashed(GameObject obj) {
		return obj.transform.parent.tag == "TrashCan";
	}

	//WARNING: this is just a console debug method, use it wisely cause it calls Destroy() 
	//		   for all pool objects!!
	public static void resetLimbo() {
		//First we destroy all objects referenced by Limbo
		//iterate the keys
		foreach(string key in Limbo.Keys) {
			//iterate the stacks
			foreach (GameObject obj in Limbo[key])
				Destroy(obj);
		}
		//Reset limbo
		Limbo.Clear();
	}

	//Disables and places obj in Limbo for reuse (or destroy).
	//Returns true if obj has been placed in Limbo. If stack for that key is full then it returns false.
	public static void TrashGameObject(GameObject obj) {
		if (obj != null && !isTrashed(obj)) {
			GameObject TrashCan = GameObject.FindGameObjectWithTag("TrashCan");
			obj.SetActive(false);
			if (TrashCan != null)
				obj.transform.SetParent(TrashCan.transform, false);

			//If name doesnt exist as key then allocate new stacklimited
			if (!Limbo.ContainsKey(obj.name))
				Limbo.Add(obj.name, new StackLimited<GameObject>(stackCapacity));
			//push it to corresponding stack. If the stack is full then destroy the obj
			if (!Limbo[obj.name].Push(obj))
					Destroy(obj);
		}
		else {
			if (obj != null)
				Debug.LogWarning("Pooling: Warning trying to trash an already trashed game object.");
		}
	}

	//Recycle from Limbo an entity with specified object name attaching it to parent GameObject
	//If stack is empty then it will instantiate new obj and return it
	public static GameObject RecycleGameObject(GameObject prefabObj, Transform parent = null) {
		GameObject newObj = null;
		if (prefabObj != null) {
			if (Limbo.ContainsKey(prefabObj.name) && !Limbo[prefabObj.name].Empty()) {
				newObj = Limbo[prefabObj.name].Pop();
				if (parent != null)
					newObj.transform.SetParent(parent, false);
				newObj.SetActive(true);
			}
			else {
				newObj = (GameObject) Instantiate(prefabObj, parent, false);
				newObj.name = prefabObj.name;
			}
		}
		return newObj;
	}
}
