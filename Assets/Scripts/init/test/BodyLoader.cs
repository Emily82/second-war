﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attached to "Player" or "BotPlayer" gameobjects
public class BodyLoader : MonoBehaviour {
	public GameObject bodyPrefab;
	// Use this for initialization
	// This code must start before other scripts (mouse camera and player movements)
	void Awake () {
		GameObject body = (GameObject) Instantiate(bodyPrefab, transform, false);
		body.name = bodyPrefab.name;
	}
}
