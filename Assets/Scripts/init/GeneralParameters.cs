﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Caliber {
	public string name;
	public string description;
	public int amount;
}

//All default general parameters are set here (default ammo equipmente, default weapons inventory, etc ...)
//Attached to Map prefab
public class GeneralParameters : MonoBehaviour {
	[Tooltip("This is the path of all prefabs available for loading, relative to Resources")]
	public string weaponsPrefabPathEdit = "weapons/real/";
	//From inspector we set default ammo equipment for EVERYONE for current map
	public Caliber[] defaultAmmoInventory;
	//TODO: add a selectable weapons prefabs array. These will be allowed weapons for the current map!

	public static GeneralParameters instance;

	//Global Parameters
	//So its available as static variable too
	public static string weaponsPrefabPath = "weapons/real/";

	void Awake() {
		instance = this;
		weaponsPrefabPath = weaponsPrefabPathEdit;
	}

	public static void setAudioSettings(ref AudioSource audio) {
		//Default audio settings
		audio.spatialBlend = 0.8f;
		audio.dopplerLevel = 0;
		audio.minDistance = 10;
		audio.maxDistance = 10;
		audio.volume = AudioListener.volume;
		audio.playOnAwake = false;
	}
}
