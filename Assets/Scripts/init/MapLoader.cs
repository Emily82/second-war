﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameMode {
	DeathMatch,
	TeamDeathMatch
};

public enum Team {
	Allies,
	Axis
}

//Loading prefabs on startup
public class MapLoader : MonoBehaviour {
	[Header("Map")]
	[Tooltip("The map we are going to load")]
	public GameObject prefabMap;

	[Header("Entities")]
	[Tooltip("Player prefab model entity")]
	public GameObject playerDefaultPrefab;
	[Tooltip("Bot prefab model entity")]
	public GameObject botPrefab;
	[Tooltip("Debug testbox model entity")]
	public GameObject debugPrefab;
	[Tooltip("Amount of bots to place in the map")]
	public uint botsAmount = 1;
	[Tooltip("Respawn GodMode time in seconds")]
	public float respawnImmunityTime = 3f;
	
	public static MapLoader instance;

	private GameObject map;
	private GameObject ground;
	private Console console;
	private Pooling pooling;
	private Entity player;
	private GameMode matchType;
	private bool playerPrefabChanged = false;

	//Map boundaries
	private float mapBoundX;
	private float mapBoundZ;

	//Respawn points
	private GameObject[] alliedPoints;
	private GameObject[] axisPoints;
	private GameObject[] allPoints;

	private Team playerTeam;
	private WeaponLoader playerWeaponLoader;

	//Time delay to disable mouse actions on respawn
	private float mouseActionLockTime = 1;

	// Use this for initialization
	void Awake () {
		instance = this;
		//
		//NOTE: Initialize input manager even if Main scene does it already. In this way
		//      we can test directly map scenes without problems. If its already initialized
		//		this function will do nothing.
		SettingsManager.Initialize();
		map = Instantiate(prefabMap, transform, false);
		map.name = prefabMap.name;
	}

	void Start () {
		matchType = GameMode.DeathMatch;
		console = Console.instance;
		pooling = Pooling.instance;
		//Retrieving all defined respawn points for this map
		alliedPoints = GameObject.FindGameObjectsWithTag("AlliesRespawn");
		axisPoints = GameObject.FindGameObjectsWithTag("AxisRespawn");
		allPoints = new GameObject[alliedPoints.Length + axisPoints.Length];
		alliedPoints.CopyTo(allPoints, 0);
		axisPoints.CopyTo(allPoints, alliedPoints.Length);

		//Get map boundaries (ground gameobject)
		ground = GameObject.FindGameObjectWithTag("Ground");
		Vector3 groundSize = ground.GetComponent<Renderer>().bounds.size;
		mapBoundX = groundSize.x;
		mapBoundZ = groundSize.z;

		//Enable Pooling
		pooling.init();

		//Lets spawn some entity
		//NOTE: at least 1 player must be created or Console will not work
		player = new Entity("player", playerDefaultPrefab, map.transform);
		//Place bots in the map
		for (int i = 0 ; i < botsAmount ; ++i)
			new Entity("debugBot" + i, botPrefab, map.transform);
		//new Entity("box", debugPrefab, map.transform);

		playerTeam = player.RootGameObject.GetComponent<EntityStatus>().team;
		playerWeaponLoader = player.RootGameObject.GetComponentInChildren<WeaponLoader>();

		//Console can now access player entity scripts after instantiation
		console.initPlayerScripts(player);
	}

	//Used by UI to change current player's model and / or weapon
	public void setPlayerPrefab(ModelPreview model) {
		playerDefaultPrefab = model.modelPrefab;
		playerTeam = model.team;
		//This will trigger the respawn to instantiate new model for player and pool the old one
		playerPrefabChanged = true;
	}

	public void setPlayerWeapon(WeaponPreview weapon) {
		playerWeaponLoader.prefabPrimaryWeapon = weapon.weaponPrefab;
	}

	//Called by console
	//NOTE: console needs primitive argument to work
	public void restart(int mode) {
		//TODO: set parameter for match type
		matchType = (GameMode)mode;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		print(matchType);
	}

	public Vector3 getNewPosition(Team team = Team.Allies) {
		GameObject[] allRespawns;
		if (matchType == GameMode.TeamDeathMatch) {
			if (team == Team.Allies)
				allRespawns = alliedPoints;
			else
				allRespawns = axisPoints;
		}
		else
			allRespawns = allPoints;

		int index = Random.Range(0, allRespawns.Length);
		return allRespawns[index].transform.position;
	}

	public void printMapBounds() {
		print("Map Size");
		print("X: " + mapBoundX);
		print("Z: " + mapBoundZ);
	}

	public void RespawnEntity(GameObject entity) {
		//We should do this only if the entity is the player and not any bot
		if (playerPrefabChanged && player.RootGameObject == entity) {
			player.TrashEntity();
			player = new Entity("player", playerDefaultPrefab, map.transform);
			//entity with new choosen model
			entity = player.RootGameObject;
			playerPrefabChanged = false;
		}
		EntityStatus entityStatus = entity.GetComponent<EntityStatus>();
		MouseCamera mouseCamera = entity.GetComponent<MouseCamera>();
		WeaponLoader weaponLoader = entity.GetComponentInChildren<WeaponLoader>();

		entityStatus.team = playerTeam;
		//Temporary disable shooting
		mouseCamera.disableActions();
		//New position
		entity.transform.position = getNewPosition(entityStatus.team);
		//Enable restore cartridge flags for all weapons
		weaponLoader.restoreAllCartridges();
		//Load weapon (all gun references for other scripts will be done by SelectPrimaryWeapon() for us)
		weaponLoader.SelectPrimaryWeapon();
		//Restore ammo
		entityStatus.restoreAmmoInventory();
		//calling enableActions after a delay
		mouseCamera.Invoke("enableActions", mouseActionLockTime);
		//Revive the entity
		entityStatus.Revive();
		print(entity.name + " respawned!");
	}
}
