﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Attached to "BotPlayer" gameobject
//This is the movement bot implementation (so not handled by user input)

//TODO: implement command movement signals for the bot
public class BotMovement : Movement {
    //Bot X axis movement
    private float deltaX = 0;
    //Bot Z axis movement
    private float deltaZ = 0;
    //Bot jumps
    private bool jump = false;

    //Start() inherited from Movement class

    void Update()
    {
        //Only if the bot is alive then do the algorithm
        if (entityStatus.IsAlive) {
            if (controller.isGrounded)
            {
                // We are grounded, so recalculate
                // move direction directly from axes
                moveDirection = new Vector3(deltaX, 0.0f, deltaZ);
                moveDirection = fpsCamera.transform.TransformDirection(moveDirection);
                moveDirection.y = 0; //So the player cant fly moving while looking up
                moveDirection = moveDirection * avatarSpeed;

                if (jump)
                {
                    moveDirection.y = maxJumpHeight;
                    jump = !jump;
                }
            }
        }

        //If we are dead move only for gravity
        if (!entityStatus.IsAlive)
            moveDirection = new Vector3(0, moveDirection.y, 0);

        // Apply gravity
        moveDirection.y = moveDirection.y + (gravity * Time.deltaTime);

        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);
    }
}