﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class will not be linked to any gameobject
public abstract class MouseCamera : MonoBehaviour {
	public float maxYAngle = 80f;

	protected float pitch, yaw;
	protected GameObject body;
	protected Transform weapon;
	protected GameObject fpsCamera;
	protected GameObject spine;
	protected Animator animator;
	protected Animator gunAnimator;
	protected Weapon gun;
	protected float recoilCount = 0;
	protected bool readyToRespawn = false;
	protected EntityStatus entityStatus;
	protected WeaponControl weaponControl;
	protected Transform lookDownWeaponTransform;
	protected Vector3 defaultWeaponPosition;
	protected Movement movement;
	protected MapLoader mapLoader;
	//This is the flag for locking shooting, bash and any other action (used during respawn)
	protected bool actionLock = false;

	//Called by BulletWeapon script on shooting
	public void Recoil() {
		//Enable recoil loop
		recoilCount = gun.recoilPitch;
		pitch -= gun.recoilPitch;
		yaw += Random.Range(-gun.recoilYaw, gun.recoilYaw);
		//Trigger weapon's recoil animation
		gunAnimator.SetTrigger("Recoil");
	}

	//Recovery after recoil
	void LateUpdate() {
		if (recoilCount > 0) {
			recoilCount -= gun.recoilRecoveryStep * Time.deltaTime;
			pitch += gun.recoilRecoveryStep * Time.deltaTime;
		}
	}

	//NOTE: This method its very important when changing weapon.
	//		If we dont update the reference to the new weapon's script, we will still send signals to the old weapon!
	public void updateGun() {
		gun = fpsCamera.GetComponentInChildren<Weapon>();
		//NOTE: When weapon changes, its animator reference must be changed too!
		gunAnimator = gun.GetComponent<Animator>();
		gun.enableAnimator();
	}

	//This Start will be usefull only for child classes
	protected void Start() {
		mapLoader = MapLoader.instance;
		fpsCamera = GetComponentInChildren<Camera>().gameObject;
		defaultWeaponPosition = fpsCamera.transform.localPosition;
		lookDownWeaponTransform = transform.Find("LookDownWeaponPos");
		gun = fpsCamera.GetComponentInChildren<Weapon>();
		
		//For complete model generally this is the Hips (root of all bones)
		body = GetComponentInChildren<ShootableEntity>().gameObject;
		entityStatus = GetComponent<EntityStatus>();
		animator = GetComponent<Animator>();
		gunAnimator = gun.GetComponent<Animator>();
		weaponControl = GetComponent<WeaponControl>();
		movement = GetComponent<Movement>();
	}

	//Called by animation event (in frontDeath animation)
	void OnDeathAnimationEnd() {
		print(this.GetType().Name + ": Death animation ended!");
		readyToRespawn = true;
		movement.resetLean();
		//To avoid to respawn in crouch mode
		animator.ResetTrigger("crouch");
	}

	public void enableActions() {
		actionLock = false;
	}

	public void disableActions() {
		actionLock = true;
	}
}
