﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//AI aimbot algorithm
//Attached to "BotPlayer" gameobject
public class BotMouseCamera : MouseCamera {
	[Tooltip("Get closer to bot more than this and ... you are dead :)")]
	public float awakeDistance = 220;
	[Tooltip("Get closer to this distance and the bot will start to bash instead of shoot")]
	public float bashAwakeDistance = 80;
	[Tooltip("Time to wait in seconds on shooting fail, before changing target.")]
	public float timeToWaitOnFail = 5;

	private struct Target {
		//hurtbox to aim
		public GameObject targetHurtbox;
		//the root hurtbox's game object
		public GameObject rootGameObject;
		public EntityStatus entityStatus;
		//Distance from target
		public float distance;
	};
	private Stack targets;
	//Hurtbox gameobject (not the root entity gameobject!)
	private Target currentTarget;
	private float timer = 0;

	// Use this for initialization
	new void Start() {
		//Call superclass Start()
		base.Start();
		//Retrieve all targets in the scene (head hurtboxes)
		detectAllTargets();
		getNextTarget();
		//Lets retrieve component in child hierarchy to shoot
		if (gun == null)
			print("BotMouseCamera: cannot shoot because BotBulletWeapon component still not available in hierarchy");
	}
	
	// left - right -> X axis
	// back - front -> Z axis
	// up	- down	-> Y axis
	// Update is called once per frame
	void Update () {
		bool isAlive = entityStatus.IsAlive;

		timer += Time.deltaTime;
		//If the target list is empty then refresh targets list
		if (targets.Count == 0)
			detectAllTargets();
		//Only if the bot is alive then do the algorithm
		if (isAlive) {
			//TODO: from here we should send a signal to movement script to reach target
			bool condition = !actionLock && gun.isFireReset && currentTarget.targetHurtbox && isCurrentTargetAlive() && isCurrentTargetInRange();
			if (condition) {
				lookAtCurrentTarget();
				if (currentTarget.distance > bashAwakeDistance)
					gun.Shoot();
				else
					gun.Bash();

				//If last shot did no damage then try with next target
				if (!gun.IsHurtboxDamaged) {
					if (timer >= timeToWaitOnFail) {
						//Attack next target hurtboxes on exceeding attempts
						getNextTarget();
						timer = 0;
					}
				}
			}
			else
				getNextTarget();
		}
		else {
			//Checking if bot can respawn
			if (readyToRespawn) {
				readyToRespawn = false;
				mapLoader.RespawnEntity(gameObject);
			}
		}
	}

	private void lookAtCurrentTarget() {
		Vector3 currentTargetPos = currentTarget.targetHurtbox.transform.position;
		//Bot auto aims current target hurtbox
		transform.LookAt(new Vector3(currentTargetPos.x,
									 transform.position.y,
									 currentTargetPos.z));
		//We need to change also camera lookat or bot will not aim perfectly
		fpsCamera.transform.LookAt(currentTargetPos);
	}

	//Select the first shootable target (generally a hurtbox object)
	private void getNextTarget() {
		bool foundAtLeastOne = false;
		//Hierarchy: BotPlayer -> BodyBot -> Head (hurtbox)
		//Exclude disabled and self owned hurtboxes
		while (targets.Count > 0) {
			GameObject target = (GameObject) targets.Pop();
			EntityStatus targetEntityStatus = target.GetComponentInParent<EntityStatus>();
			if (target != null && targetEntityStatus) {
				//Search by component its more flexible way to get the root for each target
				GameObject rootTarget = targetEntityStatus.gameObject;
				//We use player gameObject name to understand if target its self owned
				if (rootTarget && target.activeSelf && rootTarget.name != gameObject.name) 
				{
					foundAtLeastOne = true;
					//target its a gameobject owning a Hurtbox component
					currentTarget.targetHurtbox = target;
					//currentRootTarget its the root target's entity gameobject
					//We will change target root only if we found a hurtbox of a different entity root!
					if (currentTarget.rootGameObject == null || currentTarget.rootGameObject.name != rootTarget.name) {
						currentTarget.rootGameObject = rootTarget;
						currentTarget.entityStatus = targetEntityStatus;
					}
					break;
				}
			}
		}
		if (!foundAtLeastOne) {
			currentTarget.targetHurtbox = null;
			currentTarget.rootGameObject = null;
		}
	}

	private void detectAllTargets() {
		targets = new Stack(GameObject.FindGameObjectsWithTag("Hurtbox"));
	}

	private bool isCurrentTargetInRange() {
		float distance = getDistance();
		return (distance >= 0 && distance <= awakeDistance);
	}

	private bool isCurrentTargetAlive() {
		if (currentTarget.rootGameObject)
			return (currentTarget.entityStatus.IsAlive);
		else
			return false;
	}

	//Return distance between the bot and current target
	//If no current target then returns -1
	private float getDistance() {
		float distance = Vector3.Distance(currentTarget.targetHurtbox.transform.position, transform.position);
		if (currentTarget.targetHurtbox) {
			currentTarget.distance = distance;
			return distance;
		}
		else
			return -1;
	}
}
