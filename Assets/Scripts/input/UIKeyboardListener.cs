﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIKeyboardListener : MonoBehaviour {
	Weapon weapon;
	Movement movement;
	MouseCamera mouseCamera;
	Modal modal;
	Console console;

	public static UIKeyboardListener instance;

	void Awake() {
		instance = this;
		modal = Modal.instance;
	}

	void Start() {
		weapon = GetComponentInChildren<Weapon>();
		movement = GetComponent<Movement>();
		mouseCamera = GetComponent<MouseCamera>();
		console = Console.instance;
	}

	// Listening keyboard input just for opening ingame menu
	void Update () {
		if (SettingsManager.GetButtonDown("Cancel")) {
			modal.Toggle(
				() => { //Show ingameMenu -> disable player scripts
					Helper.disableUserInput(weapon, movement, mouseCamera);
					Helper.showCursor();
					console.enabled = false;
				},
				() => { //Hide ingameMenu -> enable player scripts
					Helper.enableUserInput(weapon, movement, mouseCamera);
					Helper.hideCursor();
					console.enabled = true;
				}
			);
		}
	}

	public void hideIngameMenu() {
		modal.Hide(
			() => {
				Helper.enableUserInput(weapon, movement, mouseCamera);
				Helper.hideCursor();
				console.enabled = true;
			}
		);
	}
}
