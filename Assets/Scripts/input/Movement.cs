using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class will not be attached to any gameobject
public abstract class Movement : MonoBehaviour {
    public float avatarSpeed = 5f;
    public float maxJumpHeight = 5f;
    public float gravity = Physics.gravity.y;
    [Header("Lean")]
    [Tooltip("Lean left weapon + camera horizontal shift")]
    public float leanLeftShift = 0.14f;
    [Tooltip("Lean right weapon + camera horizontal shift")]
    public float leanRightShift = 0.14f;
    [Tooltip("Lean left weapon + camera vertical shift")]
    public float leanLeftVerticalShift = -0.05f;
    [Tooltip("Lean right weapon + camera vertical shift")]
    public float leanRightVerticalShift = -0.05f;
    [Tooltip("Lean left weapon + camera front shift")]
    public float leanLeftFrontShift = 0.05f;
    [Tooltip("Lean right weapon + camera front shift")]
    public float leanRightFrontShift = -0.05f;
    [Tooltip("Lean left angle")]
    public float leanLeftAngle = 20;
    [Tooltip("Lean right angle")]
    public float leanRightAngle = 20;
    public float leanSpeed = 60; //Angular speed, degrees per seconds
    [Header("Crouch")]
    [Tooltip("Movement of camera + weapon to the bottom when crouching")]
    public float weaponCrouchShift = 1;
    [Tooltip("Delay time before crouch weapon")]
	public float crouchWeaponTime = 0.1f;

    protected Vector3 moveDirection = Vector3.zero;
    protected CharacterController controller;
    protected Transform fpsCamera;
    protected Animator animator;
    //Here we save original camera position
    protected Vector3 fpsCameraOriginalPosition;
    protected Quaternion fpsCameraOriginalRotation;
    protected EntityStatus entityStatus;
    protected WeaponLoader weaponLoader;
    protected Weapon weapon;
    protected WeaponControl weaponControl;
    protected bool switchWeaponAllowedTrigger = false;
    protected bool isWeaponDown = false;
    protected float defaultAvatarSpeed;

    //Methods called by console commands to change player's speed and jump height
    public void speed(float value) {
        avatarSpeed = value;
    }

    public void jumpSpeed(float value) {
        maxJumpHeight = value;
    }

    //Runs in child classes
    protected void Start()
    {
        controller = GetComponent<CharacterController>();
        fpsCamera = GetComponentInChildren<Camera>().transform;
        animator = GetComponent<Animator>();
        entityStatus = GetComponent<EntityStatus>();
        fpsCameraOriginalPosition = fpsCamera.localPosition;
        fpsCameraOriginalRotation = fpsCamera.localRotation;
        weaponLoader = GetComponentInChildren<WeaponLoader>();
        weaponControl = GetComponent<WeaponControl>();
        defaultAvatarSpeed = avatarSpeed;
    }

    public void resetLean() {
        //Restore to initial position considering crouch state
        if (isWeaponDown) {
            fpsCamera.localPosition = new Vector3(fpsCameraOriginalPosition.x,
                                                  fpsCameraOriginalPosition.y - weaponCrouchShift,
                                                  fpsCameraOriginalPosition.z);
        }
        else
            fpsCamera.localPosition = fpsCameraOriginalPosition;
        
        fpsCamera.localRotation = fpsCameraOriginalRotation;
    }

    //Called from OnAnimatorIK context
    protected void leanLeft() {
        Vector3 velocity = Vector3.zero;
        Vector3 fromPos = fpsCamera.localPosition;
        Quaternion fromRot = fpsCamera.localRotation;

        //We should take care if crouching or not
        float crouchShift;
        if (isWeaponDown)
            crouchShift = weaponCrouchShift;
        else
            crouchShift = 0;

        //Camera + Weapon translation and rotation
        Vector3 toPos = new Vector3(fpsCameraOriginalPosition.x - leanLeftShift,
                                    fpsCameraOriginalPosition.y + leanLeftVerticalShift - crouchShift,
                                    fpsCameraOriginalPosition.z + leanLeftFrontShift);
        Quaternion toRot = Quaternion.Euler(fpsCamera.localEulerAngles.x, 
                                            fpsCamera.localEulerAngles.y,
                                            leanLeftAngle);
        //Camera rotation
        fpsCamera.localPosition = Vector3.SmoothDamp(fromPos, toPos, ref velocity, 4 * Time.deltaTime);
        fpsCamera.localRotation = Quaternion.RotateTowards(fromRot, toRot, leanSpeed * Time.deltaTime);
        //Spine rotation
        Helper.rotateBone(animator, HumanBodyBones.Spine, leanLeftAngle, EulerAngle.Roll);
    }

    //Called from OnAnimatorIK context
    protected void leanRight() {
        Vector3 velocity = Vector3.zero;
        Vector3 fromPos = fpsCamera.localPosition;
        Quaternion fromRot = fpsCamera.localRotation;

        //We should take care if crouching or not
        float crouchShift;
        if (isWeaponDown)
            crouchShift = weaponCrouchShift;
        else
            crouchShift = 0;

        //Camera + Weapon translation and rotation
        Vector3 toPos = new Vector3(fpsCameraOriginalPosition.x + leanRightShift,
                                    fpsCameraOriginalPosition.y + leanRightVerticalShift - crouchShift,
                                    fpsCameraOriginalPosition.z + leanRightFrontShift);
        Quaternion toRot = Quaternion.Euler(fpsCamera.localEulerAngles.x, 
                                            fpsCamera.localEulerAngles.y,
                                            -leanRightAngle);

        //Camera rotation
        fpsCamera.localPosition = Vector3.SmoothDamp(fromPos, toPos, ref velocity, 4 * Time.deltaTime);
        fpsCamera.localRotation = Quaternion.RotateTowards(fromRot, toRot, leanSpeed * Time.deltaTime);
        //Spine rotation
        Helper.rotateBone(animator, HumanBodyBones.Spine, -leanRightAngle, EulerAngle.Roll);
    }

    protected void toggleCrouchState(string state) {
        animator.SetBool(state, !animator.GetBool(state));
    }

    public void allowSwitchWeapon() {
        switchWeaponAllowedTrigger = true;
    }

    //No need for onAnimatorIK context
    //Crouch or StandUp weapon called by CrouchBehaviour script
    protected void crouchWeapon() {
        if (!isWeaponDown) {
            isWeaponDown = true;
            Vector3 newPosition;
            //disable elbow ik hint on crouch
            weaponControl.disableElbowAdjust();
            newPosition = new Vector3(fpsCamera.transform.localPosition.x,
                                      fpsCamera.transform.localPosition.y - weaponCrouchShift,
                                      fpsCamera.transform.localPosition.z);
            fpsCamera.transform.localPosition = newPosition;
        }
    }

    protected void standUpWeapon() {
        if (isWeaponDown) {
            isWeaponDown = false;
            weaponControl.enableElbowAdjust();
            fpsCamera.transform.localPosition = fpsCameraOriginalPosition;
        }
    }

    protected void crouchSpeed() {
        avatarSpeed = defaultAvatarSpeed / 3;
    }

    protected void restoreSpeed() {
        avatarSpeed = defaultAvatarSpeed;
    }

    //Called by crouch behaviour script
    public void OnCrouchEvent() {
        //We can need some delay to move down the weapon on crouch
        Invoke("crouchWeapon", crouchWeaponTime);
		//On crouch we half the speed
		crouchSpeed();
    }
    public void OnStandUpEvent() {
        standUpWeapon();
		restoreSpeed();
    }
}
