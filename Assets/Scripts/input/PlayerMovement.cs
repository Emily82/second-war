﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Attached to "Player" gameobject
//User input driven implementation of Movement
public class PlayerMovement : Movement {
    //Start() inherited from Movement class


    //sets moveDirection vector setting specified animator state and parameters
    private void setMoveDirection(string rightParamName, 
                                  string leftParamName, 
                                  float rval, float lval, float speed) {
        
        //Attaching keyobard input to running Blend Tree animations
        animator.SetFloat(rightParamName, rval);
        animator.SetFloat(leftParamName, lval);

        moveDirection = new Vector3(rval, 0, lval);
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed; 
    }

    void Update()
    {
        bool isAlive = entityStatus.IsAlive;
            
        if (isAlive)
        {
            if (controller.isGrounded) {
                // We are grounded, so recalculate
                // move direction directly from axes (wasd keyboard)
                float deltaX = SettingsManager.GetAxis("Horizontal");
                float deltaZ = SettingsManager.GetAxis("Vertical");

                if (deltaX != 0 || deltaZ != 0)
                    animator.SetBool("run", true);
                else
                    animator.SetBool("run", false);

                setMoveDirection("velocityRight", "velocityForward", deltaX, deltaZ, avatarSpeed);

                //Other keyboard input movements
                if (SettingsManager.GetButtonDown("Jump")) {
                    moveDirection.y = maxJumpHeight;
                    animator.SetBool("crouch", false);
                }
            }

            //Toggle crouch animator state parameter
            if (SettingsManager.GetButtonDown("Crouch"))
                Helper.toggleParam(animator, "crouch");
                               
            //Switch weapons
            if (switchWeaponAllowedTrigger) {
                if (SettingsManager.GetButtonDown("PrimaryWeapon"))
                    weaponLoader.SelectPrimaryWeapon();
                else if (SettingsManager.GetButtonDown("SecondaryWeapon"))
                    weaponLoader.SelectSecondaryWeapon();
                else if (SettingsManager.GetButtonDown("Grenade"))
                    weaponLoader.SelectGrenadeWeapon();
            }
        }
        //If we are dead move only for gravity
        if (!entityStatus.IsAlive)
            moveDirection = new Vector3(0, moveDirection.y, 0);

        // Apply gravity
        moveDirection.y += gravity * Time.deltaTime;

        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);

        //Resets current script trigger signals for each frame
        resetAllSignals();
    }

    //Lean left-right
    void OnAnimatorIK() {
        if (entityStatus.IsAlive && animator) {
            //We can Lean even if we are not grounded
            if (SettingsManager.GetButton("LeanLeft")) {
                //Lean Left
                leanLeft();
            }
            else if(SettingsManager.GetButtonUp("LeanLeft"))
                resetLean();

            else if (SettingsManager.GetButton("LeanRight")) {
                //Lean Right
                leanRight();
            }
            else if(SettingsManager.GetButtonUp("LeanRight"))
                resetLean();
        }
    }

    void resetAllSignals() {
        switchWeaponAllowedTrigger = false;
    }
}
