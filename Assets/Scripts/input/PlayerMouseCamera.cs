using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//User input mouse aim

//Attached to "Player" gameobject
public class PlayerMouseCamera : MouseCamera {

	//Exceeding this looking down angle will push the weapon ahead
	//private float moveWeaponAngle = 60;

	// left - right -> X axis
	// back - front -> Z axis
	// up	- down	-> Y axis
	// Update is called once per frames

	new void Start() {
		Helper.hideCursor();
		base.Start();
	}

	void Update () {
		float mouseSensitivity = SettingsManager.mouseSensitivity;
		bool fire = (gun.isAutomatic())? SettingsManager.GetButton("Fire1"): SettingsManager.GetButtonDown("Fire1");
		bool bash = false;
		bool reload = false;

		//DEBUG BUTTON: uncomment to reset all binds in permanent PlayerPrefs. Press K to reset
		/* if (Input.GetKeyDown(KeyCode.K))
			InputManager.ResetPlayerPrefs(); */

		//Handling mouse cursor
		//NOTE: here we force the use of mouse button 0 to hide cursor, ignoring any bind
		if (Input.GetMouseButtonDown(0))
			Helper.hideCursor();
		if (SettingsManager.GetButtonDown("Cancel"))
			Helper.showCursor();
		
		//Wait entityStatus is initialized
		//if we are dead and we click then we can call respawn procedure
		//We can click only when readyToRespawn (death animation over)
		if (!entityStatus.IsAlive && fire && readyToRespawn) {
			readyToRespawn = false;
			mapLoader.RespawnEntity(gameObject);
		}
		
		//If i am alive then i can move the mouse
		if (entityStatus.IsAlive) {
			bash = SettingsManager.GetButtonDown("Fire2");
			reload = SettingsManager.GetButton("Fire3");
			//Mouse movements
			float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
			float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

			//Sending signals according to mouse clicks
			if (!actionLock && fire && gun.isFireReset)
				gun.Shoot();

			else if (!actionLock && bash)
				gun.Bash();

			else if (!actionLock && reload)
				gun.Reload();

			//Rotate view with mouse
			yaw += mouseX;
			pitch -= mouseY;
			yaw = Mathf.Repeat(yaw, 360);
			pitch = Mathf.Clamp(pitch, -maxYAngle, maxYAngle);

			//TODO: this code its commented for now, it creates problem with weapon movement handled
			//      by PlayerMovement script (when crouching)
			//NOTE: If pitch > moveWeaponAngle we are looking down very hard and we need 
			//      to move ahead the camera + weapon to avoid ugly right arm inverse kinematics
			/* if (pitch > moveWeaponAngle)
				forwardWeapon();
			else
				backWeapon(); */

			//Root transform rotation
			transform.localRotation = Quaternion.Euler(transform.localEulerAngles.x, 
													yaw, 
													transform.localEulerAngles.z);
			//Camera + weapon
			fpsCamera.transform.localRotation = Quaternion.Euler(pitch, 
																fpsCamera.transform.localEulerAngles.y, 
																fpsCamera.transform.localEulerAngles.z);
		}
	}


	//TODO: functions used by issue code (see upward)
	//Move ahead Camera + Weapon
	private void forwardWeapon() {
		//For weapon ahead adjust movement
		Vector3 velocity = Vector3.zero;
		fpsCamera.transform.localPosition = Vector3.SmoothDamp(fpsCamera.transform.localPosition, 
															   lookDownWeaponTransform.localPosition, 
															   ref velocity, 4 * Time.deltaTime);
	}

	//Move back Camera + Weapon
	private void backWeapon() {
		//For weapon back adjust movement
		Vector3 velocity = Vector3.zero;
		fpsCamera.transform.localPosition = Vector3.SmoothDamp(fpsCamera.transform.localPosition, 
															   defaultWeaponPosition, 
															   ref velocity, 4 * Time.deltaTime);
	}
}
